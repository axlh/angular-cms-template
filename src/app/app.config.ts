import { ApplicationConfig, importProvidersFrom } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { routes } from './app.routes';
import { provideClientHydration } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { QuillModule } from 'ngx-quill';

export const appConfig: ApplicationConfig = {
  providers: [provideClientHydration(), importProvidersFrom(RouterModule.forRoot(routes, { enableTracing: false }), BrowserAnimationsModule, QuillModule.forRoot())]
};
