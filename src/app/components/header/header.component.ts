import { Component, ViewChild, ElementRef } from '@angular/core';
import { AvatarGroupModule } from 'primeng/avatargroup';
import { AvatarModule } from 'primeng/avatar';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faBars, faSignOut, faEdit } from '@fortawesome/free-solid-svg-icons';
import { UiService } from '../../services/ui/ui.service';
import { Subscription } from 'rxjs';
import { RouterModule, Router } from '@angular/router';
import { OverlayPanelModule, OverlayPanel } from 'primeng/overlaypanel';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [ FontAwesomeModule, AvatarGroupModule, AvatarModule, RouterModule, OverlayPanelModule ],
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})


export class HeaderComponent {
  @ViewChild('panel',{static:true}) panel: OverlayPanel | undefined;
  @ViewChild('elm',{static:true}) elm: ElementRef | undefined;

  faBars = faBars;
  faSignOut = faSignOut;
  faEdit = faEdit;

  showNavbar: boolean = true;
  supscription: Subscription | undefined;

  constructor(
    private uiService: UiService,
    private router: Router
  ){
    this.supscription = this.uiService
      .onToggleNavbar()
      .subscribe((value) => {
        this.showNavbar = value;
      });
  }

  formDataEdit = {
    type: 'edit',
    data: [
      {
        id: 1,
        label: 'Status',
        type: 'select-box',
        placeholder: 'Select Status',
        required: false,
        value: 'active',
        options: [
          {
            code: 'active',
            name: 'Active'
          },
          {
            code: 'inactive',
            name: 'Inactive'
          }
        ],
        disabled: true,
        hide: true
      },
      {
        id: 2,
        label: 'Name',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Name',
        required: true,
        value: 'Developer',
        disabled: false
      },
      {
        id: 3,
        label: 'Profile Image',
        type: 'image',
        required: true,
        value: 'https://primefaces.org/cdn/primeng/images/demo/avatar/amyelsner.png',
        disabled: false
      },
      {
        id: 4,
        label: 'Email',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Email',
        required: true,
        value: 'developer@axlhnbi.com',
        disabled: false
      },
      {
        id: 5,
        label: 'Phone',
        type: 'input-text',
        inputType: 'phone',
        placeholder: 'Enter Phone Number',
        required: true,
        value: '+6281245999683',
        disabled: false
      },
      {
        id: 6,
        label: 'Last Modified by',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Last Modified by',
        required: false,
        value: '',
        disabled: true,
        hide: true
      },
      {
        id: 7,
        label: 'Last Modified',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Last Modified',
        required: false,
        value: '',
        disabled: true,
        hide: true
      },
      {
        id: 8,
        label: 'Created by',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Created by',
        required: false,
        value: '',
        disabled: true,
        hide: true
      },
      {
        id: 9,
        label: 'Created Date',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Created Date',
        required: false,
        value: '',
        disabled: true,
        hide: true
      }
    ]
  }

  toggleNavBar(){
    this.uiService.toggleNavBar();
  }

  async openEditProfile(){
    if(this.panel && this.elm) {
      this.panel.toggle(this.elm);
      this.router.navigate(['/data-form'], {
        state: {
          data: {
            module: 'Profil', 
            subModule: 'Edit Profil',
            title: 'Edit Profil',
            backRoute: this.router.url,
            formData: this.formDataEdit
          }
        }
      });
    }
  }

  logout(){
    this.router.navigate(['/login'])
  }
}
