import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { AvatarModule } from 'primeng/avatar';
import { UtilityService } from '../../services/utility/utility.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faEdit, faBan, faSearch, faTrash, faEye, faCheck, faMobileAndroidAlt, faAppleAlt, faAdd } from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';
import { UiService } from '../../services/ui/ui.service';
import { AdvancedSearchComponent } from '../advanced-search/advanced-search.component';
import { RouterModule, Router} from '@angular/router';

@Component({
  selector: 'app-table',
  standalone: true,
  imports: [ CommonModule, AvatarModule, FontAwesomeModule, AdvancedSearchComponent, RouterModule ],
  templateUrl: './table.component.html',
  styleUrl: './table.component.css'
})
export class TableComponent {
  @Input() title: any;
  @Input() data: any;
  @Input() searchInputData: any;
  @Input() addDataText: string | undefined;
  @Input() formDataInfo: any;

  isShowNavbar: boolean = true;
  isShowBtnSearch: boolean = false;
  subscription: Subscription | undefined;

  faSearch  = faSearch;
  faAdd  = faAdd;
  faEdit = faEdit;
  faBan  = faBan;
  faTrash  = faTrash;
  faEye  = faEye;
  faCheck  = faCheck;
  faMobileAndroidAlt  = faMobileAndroidAlt;
  faAppleAlt  = faAppleAlt;

  isUseNumber = true;

  constructor(
      public utility: UtilityService,
      private uiService: UiService,
      private router: Router
    ) {
      this.subscription = this.uiService
      .onToggleNavbar()
      .subscribe((value) => {
        this.isShowNavbar = value;
      });
    }

    handleShowOrHideSearchButton = () => {
      this.isShowBtnSearch = !this.isShowBtnSearch; 
    }

   openPage(index = 0, data = this.formDataInfo){
      if(data?.formData?.type === 'detail' || data?.formData?.type === 'edit'){
        for(let i = 0 ; i < data?.formData?.data.length ; i++){
          data.formData.data[i].value = data.formData.data[i].type === 'select-box' ? 
            {
              code: this.data.body[index].data[i+(this.data.body[index].data.length - data.formData.data.length)]['code'],
              name: this.data.body[index].data[i+(this.data.body[index].data.length - data.formData.data.length)]['content']
            } : 
            this.data.body[index].data[i+(this.data.body[index].data.length - data.formData.data.length)]['content'];
        }
      }

      this.router.navigate(['/data-form'], { state: {
        data: data
      }});
    }
}
