import { Component, OnInit, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { CalendarModule } from 'primeng/calendar';

interface Option {
  name: string;
  code: string;
}

@Component({
  selector: 'app-advanced-search',
  standalone: true,
  imports: [ InputTextModule, CommonModule, DropdownModule, FormsModule, FontAwesomeModule, CalendarModule ],
  templateUrl: './advanced-search.component.html',
  styleUrl: './advanced-search.component.css'
})
export class AdvancedSearchComponent implements OnInit {

  @Input() data: any;
  
  faSearch  = faSearch;

  ngOnInit(){

  }

  resetSearchInput(){
    for(let i = 0 ; i < this.data.length; i++){
      this.data[i]['value'] = '';
    }
  }

}
