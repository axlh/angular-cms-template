import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Router} from '@angular/router';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { FileUploadModule } from 'primeng/fileupload';
import { Subscription } from 'rxjs';
import { UiService } from '../../services/ui/ui.service';
import { QuillEditorComponent } from 'ngx-quill';

@Component({
  selector: 'app-data-form',
  standalone: true,
  imports: [ RouterModule, CommonModule, FormsModule, InputTextModule, DropdownModule, FileUploadModule, QuillEditorComponent ],
  templateUrl: './data-form.component.html',
  styleUrl: './data-form.component.css'
})
export class DataFormComponent {
 
  isShowNavbar: boolean = true;
  subscription: Subscription | undefined;
  uploadedFiles: any[] = [];
  data: any;
  previewImage = '';

  constructor (
    private router : Router,
    private uiService: UiService,
  ) {
    const params = this.router.getCurrentNavigation()?.extras.state;
    if(params) this.data = params['data'];
    
    this.subscription = this.uiService
    .onToggleNavbar()
    .subscribe((value) => {
      this.isShowNavbar = value;
    });
  }


  QuillConfiguration = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],
      ['blockquote', 'code-block'],
      [{ list: 'ordered' }, { list: 'bullet' }],
      [{ header: [1, 2, 3, 4, 5, 6, false] }],
      [{ color: [] }, { background: [] }],
      ['link'],
      ['clean'],
    ],
  }

  onUpload( event : any, index: number){
    this.data['formData']['data'][index]['value'] = event.files[0]['objectURL']['changingThisBreaksApplicationSecurity'];
  }

  onInputArticleChanged = (event: any, index: number) =>{
      this.data.formData.data[index].value = event.html;
  }

  onInputArticleCreated = (event: any, index: number) => {
     event.container.firstChild.innerHTML = this.data.formData.data[index].value;
  }

}
