import { Component, Input } from '@angular/core';
import { ChartModule } from 'primeng/chart';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-chart',
  standalone: true,
  imports: [ ChartModule, CommonModule ],
  templateUrl: './chart.component.html',
  styleUrl: './chart.component.css'
})
export class ChartComponent {

  @Input() title: any;
  @Input() data: any;
  @Input() options: any;
  @Input() type: any;

}
