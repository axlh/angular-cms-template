import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faChevronRight, faUserTie, faUser, faLayerGroup, faCartShopping, faListCheck, faImage } from '@fortawesome/free-solid-svg-icons';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-panel-info-icon',
  standalone: true,
  imports: [ FontAwesomeModule, CommonModule, RouterModule ],
  templateUrl: './panel-info-icon.component.html',
  styleUrl: './panel-info-icon.component.css'
})
export class PanelInfoIconComponent {

  @Input() data: any;
  
  faUserTie = faUserTie;
  faUser = faUser;
  faLayerGroup = faLayerGroup;
  faCartShopping = faCartShopping;
  faListCheck = faListCheck;
  faImage = faImage;

  faChevronRight = faChevronRight;
  
}
