import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelInfoIconComponent } from './panel-info-icon.component';

describe('PanelInfoIconComponent', () => {
  let component: PanelInfoIconComponent;
  let fixture: ComponentFixture<PanelInfoIconComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PanelInfoIconComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PanelInfoIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
