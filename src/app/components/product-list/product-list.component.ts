import { Component, Input } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faStar } from '@fortawesome/free-regular-svg-icons';
import { faCircle, faStar as faStarSolid } from '@fortawesome/free-solid-svg-icons';
import { CommonModule } from '@angular/common';
import { UtilityService } from '../../services/utility/utility.service';

@Component({
  selector: 'app-product-list',
  standalone: true,
  imports: [ FontAwesomeModule, CommonModule ],
  templateUrl: './product-list.component.html',
  styleUrl: './product-list.component.css'
})
export class ProductListComponent {

  @Input() title: any;
  @Input() data: any;

  faStar = faStar;
  faStarSolid = faStarSolid;
  faListDots = faCircle;

  constructor(
    public utility: UtilityService,
  ){

  }
}
