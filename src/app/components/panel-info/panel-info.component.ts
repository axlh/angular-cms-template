import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { UtilityService } from '../../services/utility/utility.service';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-panel-info',
  standalone: true,
  imports: [ FontAwesomeModule, CommonModule, RouterModule ],
  templateUrl: './panel-info.component.html',
  styleUrl: './panel-info.component.css'
})
export class PanelInfoComponent {

  @Input() data: any;
  faChevronRight = faChevronRight;

  constructor(
    public utility: UtilityService
  ){

  }
}
