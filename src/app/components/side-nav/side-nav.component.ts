import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {
  faDesktop,
  faUser,
  faUserTie,
  faCartShopping,
  faLayerGroup,
  faBagShopping,
  faListCheck,
  faMoneyBill,
  faMobileScreen,
  faNewspaper,
  faImage,
  faBell, 
  faQuestionCircle,
  faQuestion
} from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';
import { UiService } from '../../services/ui/ui.service';
import { RouterModule, Router} from '@angular/router';

@Component({
  selector: 'app-side-nav',
  standalone: true,
  imports: [ FontAwesomeModule, CommonModule, RouterModule ],
  templateUrl: './side-nav.component.html',
  styleUrl: './side-nav.component.css'
})
export class SideNavComponent{

  faDesktop = faDesktop;
  faUser = faUser;
  faUserTie = faUserTie;
  faCartShopping = faCartShopping;
  faLayerGroup = faLayerGroup;
  faBagShopping = faBagShopping;
  faListCheck = faListCheck;
  faMoneyBill = faMoneyBill;
  faMobileScreen = faMobileScreen;
  faNewspaper = faNewspaper;
  faBell = faBell;
  faQuestionCircle = faQuestionCircle;
  faQuestion = faQuestion;

  navItems = [
    {
      category: 'Main',
      data: [
        {
          label: 'Dashboard',
          icon: faDesktop,
          route: '/'
        }
      ]
    },
    {
      category: 'Master',
      data: [
        {
          label: 'Admin',
          icon: faUserTie,
          route: '/admin'
        },
        {
          label: 'User',
          icon: faUser,
          route: '/user'
        },
        {
          label: 'Categories',
          icon: faLayerGroup,
          route: '/categories'
        },
        {
          label: 'Product',
          icon: faBagShopping,
          route: '/product'
        },
      ]
    },
    {
      category: 'Transaction',
      data: [
        {
          label: 'Order',
          icon: faListCheck,
          route: '/order'
        },
        {
          label: 'Payment',
          icon: faMoneyBill,
          route: '/payment'
        },
      ]
    },
    {
      category: 'Content',
      data: [
        {
          label: 'Aplication Update',
          icon: faMobileScreen,
          route: '/aplication'
        },
        {
          label: 'Article',
          icon: faNewspaper,
          route: '/article'
        },
        {
          label: 'Banner',
          icon: faImage,
          route: '/banner'
        },
        {
          label: 'Broadcast',
          icon: faBell,
          route: '/broadcast'
        },
        {
          label: 'FAQ Categories',
          icon: faQuestionCircle,
          route: '/faq-categories'
        },
        {
          label: 'FAQ',
          icon: faQuestion,
          route: '/faq'
        },
      ]
    }
  ];

  showNavbar: boolean = true;
  subscription: Subscription | undefined;

  constructor(
    private uiService: UiService,
    private router: Router,
  ){
    this.subscription = this.uiService
      .onToggleNavbar()
      .subscribe((value) => {
        this.showNavbar = value;
      });
  }

  hasRoute(route: string){
    return this.router.url === route;
  }
}
