import { Routes } from '@angular/router';
import { DahsboardPage } from './pages/dashboard/dashboard.page';
import { AdminPage } from './pages/admin/admin.page';
import { UserPage } from './pages/user/user.page';
import { CategoriesPage } from './pages/categories/categories.page';
import { ProductPage } from './pages/product/product.page';
import { OrderPage } from './pages/order/order.page';
import { PaymentPage } from './pages/payment/payment.page';
import { AplicationUpdatePage } from './pages/aplication-update/aplication-update.page';
import { ArticlePage } from './pages/article/article.page';
import { BannerPage } from './pages/banner/banner.page';
import { BroadcastPage } from './pages/broadcast/broadcast.page';
import { FaqCategoriesPage } from './pages/faq-categories/faq-categories.page';
import { FaqPage } from './pages/faq/faq.page';
import { DataFormComponent } from './components/data-form/data-form.component';
import { LoginPage } from './pages/login/login.page';

export const routes: Routes = [
    {
        path: '',
        component: DahsboardPage
    },
    {
        path: 'login',
        component: LoginPage
    },
    {
        path: 'admin',
        component: AdminPage
    },
    {
        path: 'user',
        component: UserPage
    },
    {
        path: 'categories',
        component: CategoriesPage
    },
    {
        path: 'product',
        component: ProductPage
    },
    {
        path: 'order',
        component: OrderPage
    },
    {
        path: 'payment',
        component: PaymentPage
    },
    {
        path: 'aplication',
        component: AplicationUpdatePage
    },
    {
        path: 'article',
        component: ArticlePage
    },
    {
        path: 'banner',
        component: BannerPage
    },
    {
        path: 'broadcast',
        component: BroadcastPage
    },
    {
        path: 'faq-categories',
        component: FaqCategoriesPage
    },
    {
        path: 'faq',
        component: FaqPage
    },
    {
        path: 'data-form',
        component: DataFormComponent
    }
];
