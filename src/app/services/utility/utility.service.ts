import { Injectable } from '@angular/core';
import sanitizeHtml from 'sanitize-html';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  constructor() { }

  set3Dot(text: string, length: number = 30){
    if(text && text.length >= length){
      return text = text.slice(0,length)+'...'
    }
    return text
  }

  doSanitizeHtml(content: string){
    return sanitizeHtml(content);
  }

  setCurrency(price: bigint){
    return `$ ${new Intl.NumberFormat('en-US').format(price)}`
  }

}
