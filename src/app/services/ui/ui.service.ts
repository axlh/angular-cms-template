import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UiService {
  private showNavBar: boolean = true;
  private subject = new Subject<any>();
  
  constructor(
  ) { }

  toggleNavBar(): void{
    this.showNavBar = !this.showNavBar;
    this.subject.next(this.showNavBar);
  }

  onToggleNavbar(){
    return this.subject.asObservable();
  }
}
