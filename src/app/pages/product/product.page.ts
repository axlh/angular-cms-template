import { Component } from '@angular/core';
import { TableComponent } from '../../components/table/table.component';
import { UtilityService } from '../../services/utility/utility.service';

@Component({
  selector: 'app-product',
  standalone: true,
  imports: [ TableComponent ],
  templateUrl: './product.page.html',
  styleUrl: './product.page.css'
})
export class ProductPage {
  constructor(public utility: UtilityService){

  }

  formData = {
    type: 'add',
    data: [
      {
        id: 1,
        label: 'Product Name',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Product Name',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 2,
        label: 'Price',
        type: 'input-text',
        inputType: 'number',
        placeholder: 'Enter Price',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 3,
        label: 'Material',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Material',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 4,
        label: 'Weight',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Weight',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 5,
        label: 'Use Type',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Use Type',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 6,
        label: 'Tag Line',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Tag Line',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 7,
        label: 'Design Type',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Tag Design Type',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 8,
        label: 'Image',
        type: 'image',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 9,
        label: 'Description',
        type: 'input-textarea',
        inputType: 'text',
        placeholder: 'Enter Description',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 10,
        label: 'Category Name',
        type: 'select-box',
        placeholder: 'Select Category',
        required: true,
        value: '',
        options: [
          {
            code: 'men',
            name: 'For Men'
          },
          {
            code: 'women',
            name: 'For Women'
          }
        ],
        disabled: false
      },
      {
        id: 11,
        label: 'Type',
        type: 'select-box',
        placeholder: 'Select Type',
        required: true,
        value: '',
        options: [
          {
            code: 'mentop',
            name: "Men's Top"
          },
          {
            code: 'womenbottom',
            name: "Women's Bottom"
          }
        ],
        disabled: false
      },
      {
        id: 12,
        label: 'Gender',
        type: 'select-box',
        placeholder: 'Select Gender',
        required: true,
        value: '',
        options: [
          {
            code: 'male',
            name: "Male"
          },
          {
            code: 'female',
            name: "Female"
          }
        ],
        disabled: false
      },
      {
        id: 13,
        label: 'Size',
        type: 'select-box',
        placeholder: 'Select Size',
        required: true,
        value: '',
        options: [
          {
            code: 's',
            name: "S"
          },
          {
            code: 'm',
            name: 'M'
          },
          {
            code: 'l',
            name: "L"
          },
          {
            code: 'xl',
            name: "XL"
          },
          {
            code: 'xxl',
            name: "XXL"
          }
        ],
        disabled: false
      },
      {
        id: 14,
        label: 'Color',
        type: 'select-box',
        placeholder: 'Select Color',
        required: true,
        value: '',
        options: [
          {
            code: 'white',
            name: 'White'
          },
          {
            code: 'black',
            name: 'Black'
          },
          {
            code: 'red',
            name: 'Red'
          },
          {
            code: 'blue',
            name: 'Blue'
          },
        ],
        disabled: false
      },
      {
        id: 15,
        label: 'Status',
        type: 'select-box',
        placeholder: 'Select Status',
        required: true,
        value: '',
        options: [
          {
            code: 'active',
            name: 'Active'
          },
          {
            code: 'inactive',
            name: 'Inactive'
          }
        ],
        disabled: false
      }
    ]
  }

  formDataEdit = {
    type: 'edit',
    data: [
      {
        id: 1,
        label: 'Status',
        type: 'select-box',
        placeholder: 'Select Status',
        required: false,
        value: '',
        options: [
          {
            code: 'active',
            name: 'Active'
          },
          {
            code: 'inactive',
            name: 'Inactive'
          }
        ],
        disabled: true,
        hide: true
      },
      {
        id: 2,
        label: 'Product Name',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Product Name',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 3,
        label: 'Price',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Price',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 4,
        label: 'Rate',
        type: 'select-box',
        placeholder: 'Select Rate',
        required: false,
        value: '',
        options: [
          {
            code: '1',
            name: '1'
          },
          {
            code: '2',
            name: '2'
          },
          {
            code: '3',
            name: '3'
          },
          {
            code: '4',
            name: '4'
          },
          {
            code: '5',
            name: '5'
          }
        ],
        disabled: true,
        hide: true
      },
      {
        id: 5,
        label: 'Material',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Material',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 6,
        label: 'Weight',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Weight',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 7,
        label: 'Use Type',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Use Type',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 8,
        label: 'Tag Line',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Tag Line',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 9,
        label: 'Design Type',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Design Type',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 10,
        label: 'Image',
        type: 'image',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 10,
        label: 'Description',
        type: 'input-textarea',
        inputType: 'text',
        placeholder: 'Enter Description',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 11,
        label: 'Category Name',
        type: 'select-box',
        placeholder: 'Select Category Name',
        required: false,
        value: '',
        options: [
          {
            code: 'men',
            name: 'For Men'
          },
          {
            code: 'women',
            name: 'For Women'
          }
        ],
        disabled: false
      },
      {
        id: 12,
        label: 'Type',
        type: 'select-box',
        placeholder: 'Select Type',
        required: false,
        value: '',
        options: [
          {
            code: 'mentop',
            name: "Men's Top"
          },
          {
            code: 'womenbottom',
            name: "Women's Bottom"
          }
        ],
        disabled: false
      },
      {
        id: 13,
        label: 'Number of Items',
        type: 'input-text',
        inputType: 'number',
        placeholder: 'Enter Number of Items',
        required: false,
        value: '',
        disabled: true,
        hide: true
      },
      {
        id: 14,
        label: 'Gender',
        type: 'select-box',
        placeholder: 'Select Gender',
        required: false,
        value: '',
        options: [
          {
            code: 'male',
            name: "Male"
          },
          {
            code: 'female',
            name: "Female"
          }
        ],
        disabled: false
      },
      {
        id: 15,
        label: 'Size',
        type: 'select-box',
        placeholder: 'Select Size',
        required: false,
        value: '',
        options: [
          {
            code: 's',
            name: 'S'
          },
          {
            code: 'm',
            name: 'M'
          },
          {
            code: 'l',
            name: "L"
          },
          {
            code: 'xl',
            name: "XL"
          },
          {
            code: 'xxl',
            name: "XXL"
          }
        ],
        disabled: false
      },
      {
        id: 16,
        label: 'Color',
        type: 'select-box',
        placeholder: 'Select Color',
        required: false,
        value: '',
        options: [
          {
            code: 'white',
            name: 'White'
          },
          {
            code: 'black',
            name: 'Black'
          },
          {
            code: 'red',
            name: 'Red'
          },
          {
            code: 'blue',
            name: 'Blue'
          }
        ],
        disabled: false
      },
      {
        id: 17,
        label: 'Last Modified by',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Last Modified by',
        required: false,
        value: '',
        disabled: true,
        hide: true
      },
      {
        id: 18,
        label: 'Last Modified',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Last Modified',
        required: false,
        value: '',
        disabled: true,
        hide: true
      },
      {
        id: 19,
        label: 'Created by',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Created by',
        required: false,
        value: '',
        disabled: true,
        hide: true
      },
      {
        id: 20,
        label: 'Created Date',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Created Date',
        required: false,
        value: '',
        disabled: true,
        hide: true
      }
    ]
  }

  data = {
    head: [
      {
        text: '#',
      },
      {
        text: 'Action'
      },
      {
        text: 'Status'
      },
      {
        text: 'Product Name',
      },
      {
        text: 'Price'
      },
      {
        text: 'Rate'
      },
      {
        text: 'Material'
      },
      {
        text: 'Weight'
      },
      {
        text: 'Use Type'
      },
      {
        text: 'Tag line'
      },
      {
        text: 'Design Type'
      },
      {
        text: 'Image'
      },
      {
        text: 'Descriptioin'
      },
      {
        text: 'Category Name',
      },
      {
        text: 'Type'
      },
      {
        text: 'Number of Items'
      },
      {
        text: 'Gender'
      },
      {
        text: 'Size'
      },
      {
        text: 'Color'
      },
      {
        text: 'Last Modified by',
      },
      {
        text: 'Last Modified',
      },
      {
        text: 'Created By',
      },
      {
        text: 'Created Date',
      }
    ],
    body: [
      {
        id: 1,
        data: [
          {
            type: 'buttons',
            buttons: [
              {
                id: 1,
                text: 'Edit',
                class: 'btn btn-primary',
                props: {
                        module: 'Product', 
                        subModule: 'Edit Product',
                        title: 'Edit Product',
                        backRoute: '/product',
                        formData: this.formDataEdit
                       },
                action: 'openPage',
                icon: 'faEdit'
              },
              {
                id: 2,
                text: 'Delete',
                class: 'btn btn-secondary',
                icon: 'faTrash'
              },
              {
                id: 3,
                text: 'Set to Inactive',
                class: 'btn btn-danger',
                icon: 'faBan'
              }
            ]
          },
          {
            type: 'status',
            class: 'btn btn-success',
            content: 'Active'
          },
          {
            type: 'text',
            content: 'GLacier Steal Jacket'
          },
          {
            type: 'text',
            content: this.utility.setCurrency(BigInt(30))
          },
          {
            type: 'text',
            content: 5,
            code: 5
          },
          {
            type: 'text',
            content: '100% Cotton'
          },
          {
            type: 'text',
            content: 'Light weight'
          },
          {
            type: 'text',
            content: 'Casual'
          },
          {
            type: 'text',
            content: 'Best finish'
          },
          {
            type: 'text',
            content: 'Unique design'
          },
          {
            type: 'avatar',
            content: 'https://sam-nyc.com/cdn/shop/products/GLACIER_STEEL_019_600x.jpg?v=1634676604',
            size: 'large'
          },
          {
            type: 'innerContent',
            content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint, dolore. Ipsum blanditiis, <br> quo cumque praesentium commodi eligendi sit odit maiores corporis modi similique iste, <br>esse ipsa culpa debitis eaque. Modi.',
            sort: 60
          },{
            type: 'text',
            content: 'For Men',
            code: 'men'
          },
          {
            type: 'text',
            content: "Men's Top",
            code: 'mentop'
          },{
            type: 'text',
            content: 30
          },
          {
            type: 'text',
            content: 'Male',
            code: 'male'
          },
          {
            type: 'text',
            content: 'L',
            code: 'l'
          },
          {
            type: 'text',
            content: 'Black',
            code: 'black'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          }
        ]
      },
      {
        id: 2,
        data: [
          {
            type: 'buttons',
            buttons: [
              {
                id: 1,
                text: 'Edit',
                class: 'btn btn-primary',
                props: {
                        module: 'Product', 
                        subModule: 'Edit Product',
                        title: 'Edit Product',
                        backRoute: '/product',
                        formData: this.formDataEdit
                       },
                action: 'openPage',
                icon: 'faEdit'
              },
              {
                id: 2,
                text: 'Delete',
                class: 'btn btn-secondary',
                icon: 'faTrash'
              },
              {
                id: 3,
                text: 'Set to Inactive',
                class: 'btn btn-danger',
                icon: 'faBan'
              }
            ]
          },
          {
            type: 'status',
            class: 'btn btn-success',
            content: 'Active'
          },
          {
            type: 'text',
            content: 'Hight Wist Jeans'
          },
          {
            type: 'text',
            content: this.utility.setCurrency(BigInt(40))
          },
          {
            type: 'text',
            content: 4
          },
          {
            type: 'text',
            content: '100% Jeans'
          },
          {
            type: 'text',
            content: 'Light weight'
          },
          {
            type: 'text',
            content: 'Casual'
          },
          {
            type: 'text',
            content: 'Best finish'
          },
          {
            type: 'text',
            content: 'Unique design'
          },
          {
            type: 'avatar',
            content: 'https://www.versace.com/dw/image/v2/BGWN_PRD/on/demandware.static/-/Sites-ver-master-catalog/default/dw9786da1c/original/90_E75HAB508-EDW023L54_E904_10_High~Waist~Jeans-Pants-Versace-online-store_1_0.jpg?sw=850&q=85&strip=true',
            size: 'large'
          },
          {
            type: 'innerContent',
            content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint, dolore. Ipsum blanditiis, <br> quo cumque praesentium commodi eligendi sit odit maiores corporis modi similique iste, <br>esse ipsa culpa debitis eaque. Modi.',
            sort: 60
          },{
            type: 'text',
            content: 'For Women',
            code: 'women'
          },
          {
            type: 'text',
            content: "Women's Bottom",
            code: 'womenbottom'
          },{
            type: 'text',
            content: 12
          },
          {
            type: 'text',
            content: 'Female',
            code: 'female'
          },
          {
            type: 'text',
            content: 'S',
            code: 's'
          },
          {
            type: 'text',
            content: 'Blue',
            code: 'blue'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          }
        ]
      }
    ]
  }

  searchInputData = [
    {
      id: 1,
      label: 'Product Name',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'Product Name',
      value: ''
    },
    {
      id: 2,
      label: 'Price',
      type: 'input-text',
      inputType: 'number',
      placeholder: 'Price',
      value: ''
    },
    {
      id: 3,
      label: 'Rate',
      type: 'select-box',
      placeholder: 'Select Rate',
      value: '',
      options: [
        {
          code: '1',
          name: '1'
        },
        {
          code: '2',
          name: '2'
        },
        {
          code: '3',
          name: '3'
        },
        {
          code: '4',
          name: '4'
        },
        {
          code: '5',
          name: '5'
        },
      ]
    },
    {
      id: 4,
      label: 'Material',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'Material',
      value: ''
    },
    {
      id: 5,
      label: 'Weight',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'Weight',
      value: ''
    },
    {
      id: 6,
      label: 'Use Type',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'Use Type',
      value: ''
    },
    {
      id: 7,
      label: 'Tag Line',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'Tag Line',
      value: ''
    },
    {
      id: 8,
      label: 'Design Type',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'Design Type',
      value: ''
    },
    {
      id: 9,
      label: 'Category Name',
      type: 'select-box',
      placeholder: 'Select Category',
      value: '',
      options: [
        {
          code: 'men',
          name: 'For Men'
        },
        {
          code: 'women',
          name: 'For Women'
        }
      ]
    },
    {
      id: 10,
      label: 'Type',
      type: 'select-box',
      placeholder: 'Select Type',
      value: '',
      options: [
        {
          code: 'mentop',
          name: "Men's Top"
        },
        {
          code: 'womenbottom',
          name: "Women's Bottom"
        }
      ]
    },
    {
      id: 11,
      label: 'Gender',
      type: 'select-box',
      placeholder: 'Select Gender',
      value: '',
      options: [
        {
          code: 'male',
          name: "Male"
        },
        {
          code: 'female',
          name: "Female"
        }
      ]
    },
    {
      id: 12,
      label: 'Size',
      type: 'select-box',
      placeholder: 'Select Size',
      value: '',
      options: [
        {
          code: 's',
          name: "S"
        },
        {
          code: 'm',
          name: 'M'
        },
        {
          code: 'l',
          name: "L"
        },
        {
          code: 'xl',
          name: "XL"
        },
        {
          code: 'xxl',
          name: "XXL"
        }
      ]
    },
    {
      id: 13,
      label: 'Color',
      type: 'select-box',
      placeholder: 'Select Color',
      value: '',
      options: [
        {
          code: 'white',
          name: 'White'
        },
        {
          code: 'black',
          name: 'Black'
        },
        {
          code: 'red',
          name: 'Red'
        },
        {
          code: 'blue',
          name: 'Blue'
        }
      ]
    },
    {
      id: 14,
      label: 'Status',
      type: 'select-box',
      placeholder: 'Select Status',
      value: '',
      options: [
        {
          code: 'active',
          name: 'Active'
        },
        {
          code: 'inactive',
          name: 'Inactive'
        }
      ]
    }
   ]
}
