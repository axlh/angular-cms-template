import { Component } from '@angular/core';
import { UtilityService } from '../../services/utility/utility.service';
import { TableComponent } from '../../components/table/table.component';

@Component({
  selector: 'app-payment',
  standalone: true,
  imports: [ TableComponent ],
  templateUrl: './payment.page.html',
  styleUrl: './payment.page.css'
})
export class PaymentPage {

  constructor(
    public utility: UtilityService
  ){}

  data = {
    head: [
      {
        text: '#'
      },
      {
        text: 'Action'
      },
      {
        text: 'Status'
      },
      {
        text: 'Category'
      },
      {
        text: 'Payment Method Code'
      },
      {
        text: 'Payment Method Name'
      },
      {
        text: 'Last Modified by',
      },
      {
        text: 'Last Modified',
      },
      {
        text: 'Created By',
      },
      {
        text: 'Created Date',
      }
    ],
    body: [
      {
        id: 1,
        data: [
          {
            type: 'buttons',
            buttons : [
              {
                id: 1,
                text: 'Set to Inactive',
                class: 'btn btn-danger',
                icon: 'faBan'
              }
            ]
          },
          {
            type: 'status',
            class: 'btn btn-success',
            content: 'Active'
          },
          {
            type: 'text',
            content: 'Fintech'
          },
          {
            type: 'text',
            content: 'OVO'
          },
          {
            type: 'text',
            content: 'OVO'
          },
          {
            type: 'text',
            content: 'Kylian Mbappe'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          },
          {
            type: 'text',
            content: 'Kylian Mbappe'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          }
        ]
      },
      {
        id: 2,
        data: [
          {
            type: 'buttons',
            buttons : [
              {
                id: 1,
                text: 'Set to Inactive',
                class: 'btn btn-danger',
                icon: 'faBan'
              }
            ]
          },
          {
            type: 'status',
            class: 'btn btn-success',
            content: 'Active'
          },
          {
            type: 'text',
            content: 'M Banking'
          },
          {
            type: 'text',
            content: 'M-Banking Mandiri'
          },
          {
            type: 'text',
            content: 'M Banking Mandiri'
          },
          {
            type: 'text',
            content: 'Marcus Rashford'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          },
          {
            type: 'text',
            content: 'Marcus Rashford'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          }
        ]
      }
    ]
  }

  searchInputData = [
    {
      id: 1,
      label: 'Category',
      type: 'select-box',
      placeholder: 'Select Category',
      value: '',
      options: [
        {
          code: 'fintech',
          name: 'Fintech'
        },
        {
          name: 'M Banking',
          code: 'mbanking'
        }
      ]
    },
    {
      id: 2,
      label: 'Payment Method Code',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'Payment Method Code',
      value: ''
    },
    {
      id: 3,
      label: 'Payment Method Name',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'Payment Method Name',
      value: ''
    },
    {
      id: 4,
      label: 'Status',
      type: 'select-box',
      placeholder: 'Select Status',
      value: '',
      options: [
        {
          code: 'active',
          name: 'Active'
        },
        {
          code: 'inactive',
          name: 'Inactive'
        }
      ]
    }
   ]
}
