import { Component } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import { RouterModule, Router} from '@angular/router';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [ FontAwesomeModule, RouterModule ],
  templateUrl: './login.page.html',
  styleUrl: './login.page.css'
})
export class LoginPage {

  faEye = faEye;
  faEyeSlash = faEyeSlash;

  inputPassType = 'password';

  changeInputPassType(){
    if(this.inputPassType === 'password'){
      this.inputPassType = 'text';
    }else{
      this.inputPassType = 'password';
    }
  }
}
