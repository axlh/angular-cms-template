import { Component } from '@angular/core';
import { TableComponent } from '../../components/table/table.component';
import { UtilityService } from '../../services/utility/utility.service';

@Component({
  selector: 'app-broadcast',
  standalone: true,
  imports: [ TableComponent ],
  templateUrl: './broadcast.page.html',
  styleUrl: './broadcast.page.css'
})
export class BroadcastPage {
  constructor(
    public utility: UtilityService
  ){
    
  }

  formData = {
    type: 'add',
    data: [
      {
        id: 1,
        label: 'Type',
        type: 'select-box',
        placeholder: 'Select Type',
        required: true,
        value: '',
        options: [
          {
            code: 'now',
            name: 'Now'
          },
          {
            code: 'later',
            name: 'Later'
          }
        ],
        disabled: false
      },
      {
        id: 2,
        label: 'Title',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Title',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 3,
        label: 'Headline',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Headline',
        required: true,
        value: '',
        disabled: false
      }
    ]
  }

  formDataDetail = {
    type: 'detail',
    data: [
      {
        id: 1,
        label: 'Status',
        type: 'select-box',
        placeholder: 'Select Status',
        required: false,
        value: '',
        options: [
          {
            code: 'sent',
            name: 'Sent'
          },
          {
            code: 'inprogress',
            name: 'Inprogress'
          },
          {
            code: 'cancel',
            name: 'Cancel'
          }
        ],
        disabled: true,
        hide: true
      },
      {
        id: 2,
        label: 'Type',
        type: 'select-box',
        placeholder: 'Select Type',
        required: true,
        value: '',
        options: [
          {
            code: 'now',
            name: 'Now'
          },
          {
            code: 'later',
            name: 'Later'
          }
        ],
        disabled: false
      },
      {
        id: 3,
        label: 'Title',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Title',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 3,
        label: 'Headline',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Headline',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 4,
        label: 'Created by',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Created by',
        required: false,
        value: '',
        disabled: true,
        hide: true
      },
      {
        id: 5,
        label: 'Created Date',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Created Date',
        required: false,
        value: '',
        disabled: true,
        hide: true
      }
    ]
  }

  data = {
    head: [
      {
        text: '#'
      },
      {
        text: 'Action'
      },
      {
        text: 'Send Status'
      },
      {
        text: 'Type'
      },
      {
        text: 'Title'
      },
      {
        text: 'Headline'
      },
      {
        text: 'Created By',
      },
      {
        text: 'Created Date',
      }
    ],
    body: [
      {
        id: 1,
        data: [
          {
            type: 'buttons',
            buttons: [
              {
                id: 1,
                text: 'Detail',
                class: 'btn btn-info text-light',
                routerLink: '/data-form',
                action: 'openPage',
                icon: 'faEye',
                props: {
                  module: 'Broadcast', 
                  subModule: 'Detail Broadcast',
                  title: 'Detail Broadcast',
                  backRoute: '/broadcast',
                  formData: this.formDataDetail
                 }
              },
              {
                id: 2,
                text: 'Delete',
                class: 'btn btn-secondary',
                icon: 'faTrash'
              }
            ]
          },
          {
            type: 'status',
            class: 'btn btn-success',
            content: 'Sent'
          },
          {
            type: 'text',
            content: 'Now',
            code: 'now'
          },
          {
            type: 'text',
            content: 'Test Notification'
          },
          {
            type: 'text',
            content: 'Test Notification'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          }
        ]
      },
      {
        id: 2,
        data: [
          {
            type: 'buttons',
            buttons: [
              {
                id: 1,
                text: 'Detail',
                class: 'btn btn-info text-light',
                routerLink: '/data-form',
                action: 'openPage',
                icon: 'faEye',
                props: {
                  module: 'Broadcast', 
                  subModule: 'Detail Broadcast',
                  title: 'Detail Broadcast',
                  backRoute: '/broadcast',
                  formData: this.formDataDetail
                 }
              },
              {
                id: 2,
                text: 'Delete',
                class: 'btn btn-secondary',
                icon: 'faTrash'
              }
            ]
          },
          {
            type: 'status',
            class: 'btn btn-danger',
            content: 'Cancel'
          },
          {
            type: 'text',
            content: 'Now',
            code: 'now'
          },
          {
            type: 'text',
            content: 'New Notification'
          },
          {
            type: 'text',
            content: 'New Notification'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          }
        ]
      },
      {
        id: 3,
        data: [
          {
            type: 'buttons',
            buttons: [
              {
                id: 1,
                text: 'Detail',
                class: 'btn btn-info text-light',
                routerLink: '/data-form',
                action: 'openPage',
                icon: 'faEye',
                props: {
                  module: 'Broadcast', 
                  subModule: 'Detail Broadcast',
                  title: 'Detail Broadcast',
                  backRoute: '/broadcast',
                  formData: this.formDataDetail
                 }
              },
              {
                id: 2,
                text: 'Delete',
                class: 'btn btn-secondary',
                icon: 'faTrash'
              }
            ]
          },
          {
            type: 'status',
            class: 'btn btn-warning',
            content: 'In Progress'
          },
          {
            type: 'text',
            content: 'Later',
            code: 'later'
          },
          {
            type: 'text',
            content: 'Product Sold'
          },
          {
            type: 'text',
            content: 'Product Sold Out'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          }
        ]
      }
    ]
  }

  searchInputData = [
    {
      id: 1,
      label: 'Status',
      type: 'select-box',
      placeholder: 'Select Status',
      value: '',
      options: [
        {
          code: 'sent',
          name: 'Sent'
        },
        {
          code: 'inprogress',
          name: 'Inprogress'
        },
        {
          code: 'cancel',
          name: 'Cancel'
        }
      ]
    },
    {
      id: 2,
      label: 'Type',
      type: 'select-box',
      placeholder: 'Select Type',
      value: '',
      options: [
        {
          code: 'now',
          name: 'Now'
        },
        {
          code: 'later',
          name: 'Later'
        }
      ]
    },
    {
      id: 3,
      label: 'Title',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'Title',
      value: ''
    },
    {
      id: 4,
      label: 'Headline',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'headline',
      value: ''
    }
   ]
}
