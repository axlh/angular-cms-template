import { Component } from '@angular/core';
import { TableComponent } from '../../components/table/table.component';
import { UtilityService } from '../../services/utility/utility.service';

@Component({
  selector: 'app-banner',
  standalone: true,
  imports: [ TableComponent ],
  templateUrl: './banner.page.html',
  styleUrl: './banner.page.css'
})
export class BannerPage {
  constructor(
    public utility: UtilityService
  ){

  }

  formData = {
    type: 'add',
    data: [
      {
        id: 1,
        label: 'Image',
        type: 'image',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 2,
        label: 'Banner Name',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Banner Name',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 3,
        label: 'Banner Type',
        type: 'select-box',
        placeholder: 'Select Banner Type',
        required: true,
        value: '',
        options: [
          {
            code: 'all',
            name: 'All'
          },
          {
            code: 'country',
            name: 'Country'
          },
          {
            code: 'province',
            name: 'Province'
          },
          {
            code: 'city',
            name: 'City'
          }
        ],
        disabled: false
      },
      {
        id: 4,
        label: 'Status',
        type: 'select-box',
        placeholder: 'Select Status',
        required: true,
        value: '',
        options: [
          {
            code: 'active',
            name: 'Active'
          },
          {
            code: 'inactive',
            name: 'Inactive'
          }
        ],
        disabled: false
      }
    ]
  }

  formDataEdit = {
    type: 'edit',
    data: [
      {
        id: 1,
        label: 'Status',
        type: 'select-box',
        placeholder: 'Select Status',
        required: false,
        value: '',
        options: [
          {
            code: 'active',
            name: 'Active'
          },
          {
            code: 'inactive',
            name: 'Inactive'
          }
        ],
        disabled: true,
        hide: true
      },
      {
        id: 2,
        label: 'Image',
        type: 'image',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 3,
        label: 'Banner Name',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Banner Name',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 4,
        label: 'Banner Type',
        type: 'select-box',
        placeholder: 'Select Banner Type',
        required: false,
        value: '',
        options: [
          {
            code: 'all',
            name: 'All'
          },
          {
            code: 'country',
            name: 'Country'
          },
          {
            code: 'province',
            name: 'Province'
          },
          {
            code: 'city',
            name: 'City'
          }
        ],
        disabled: false,
        hide: false
      },
      {
        id: 5,
        label: 'Last Modified by',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Last Modified by',
        required: false,
        value: '',
        disabled: true,
        hide: true
      },
      {
        id: 6,
        label: 'Last Modified',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Last Modified',
        required: false,
        value: '',
        disabled: true,
        hide: true
      },
      {
        id: 7,
        label: 'Created by',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Created by',
        required: false,
        value: '',
        disabled: true,
        hide: true
      },
      {
        id: 8,
        label: 'Created Date',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Created Date',
        required: false,
        value: '',
        disabled: true,
        hide: true
      }
    ]
  }

  data = {
    head: [
      {
        text: '#'
      },
      {
        text: 'Action'
      },
      {
        text: 'Status'
      },
      {
        text: 'Image'
      },
      {
        text: 'Banner Name'
      },
      {
        text: 'Banner Type'
      },
      {
        text: 'Last Modified by',
      },
      {
        text: 'Last Modified',
      },
      {
        text: 'Created By',
      },
      {
        text: 'Created Date',
      }
    ],
    body: [
      {
        id: 1,
        data: [
          {
            type: 'buttons',
            buttons: [
              {
                id: 1,
                text: 'Edit',
                class: 'btn btn-primary',
                props: {
                        module: 'Banner', 
                        subModule: 'Edit Banner',
                        title: 'Edit Banner',
                        backRoute: '/banner',
                        formData: this.formDataEdit
                       },
                action: 'openPage',
                icon: 'faEdit'
              },
              {
                id: 2,
                text: 'Delete',
                class: 'btn btn-secondary',
                icon: 'faTrash'
              },
              {
                id: 3,
                text: 'Set to Inactive',
                class: 'btn btn-danger',
                icon: 'faBan'
              }
            ]
          },
          {
            type: 'status',
            class: 'btn btn-success',
            content: 'Active'
          },
          {
            type: 'avatar',
            content: 'https://th.bing.com/th/id/R.78f6c7e170c2aa3027e4d4d9e8a9bedc?rik=2EC%2fcZeaJSpFiw&riu=http%3a%2f%2fimages2.fanpop.com%2fimages%2fphotos%2f3000000%2fArshavin-wallpaper-andrei-arshavin-3031250-800-600.jpg&ehk=VMaTiyXuMCh5PHqCbFZt2zo8DCgHc3uW0jr3Vx2zbbE%3d&risl=&pid=ImgRaw&r=0',
            shape: 'circle',
            size: 'large'
          },
          {
            type: 'text',
            content: 'Banner'
          },
          {
            type: 'text',
            content: 'All',
            code: 'all'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          }
        ]
      },
      {
        id: 2,
        data: [
          {
            type: 'buttons',
            buttons: [
              {
                id: 1,
                text: 'Edit',
                class: 'btn btn-primary',
                props: {
                        module: 'Banner', 
                        subModule: 'Edit Banner',
                        title: 'Edit Banner',
                        backRoute: '/banner',
                        formData: this.formDataEdit
                       },
                action: 'openPage',
                icon: 'faEdit'
              },
              {
                id: 2,
                text: 'Delete',
                class: 'btn btn-secondary',
                icon: 'faTrash'
              },
              {
                id: 3,
                text: 'Set to Inactive',
                class: 'btn btn-danger',
                icon: 'faBan'
              }
            ]
          },
          {
            type: 'status',
            class: 'btn btn-success',
            content: 'Active'
          },
          {
            type: 'avatar',
            content: 'https://th.bing.com/th/id/R.78f6c7e170c2aa3027e4d4d9e8a9bedc?rik=2EC%2fcZeaJSpFiw&riu=http%3a%2f%2fimages2.fanpop.com%2fimages%2fphotos%2f3000000%2fArshavin-wallpaper-andrei-arshavin-3031250-800-600.jpg&ehk=VMaTiyXuMCh5PHqCbFZt2zo8DCgHc3uW0jr3Vx2zbbE%3d&risl=&pid=ImgRaw&r=0',
            shape: 'circle',
            size: 'large'
          },
          {
            type: 'text',
            content: 'Banner 2'
          },
          {
            type: 'text',
            content: 'City',
            code: 'city'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          }
        ]
      },
      {
        id: 3,
        data: [
          {
            type: 'buttons',
            buttons: [
              {
                id: 1,
                text: 'Edit',
                class: 'btn btn-primary',
                props: {
                        module: 'Banner', 
                        subModule: 'Edit Banner',
                        title: 'Edit Banner',
                        backRoute: '/banner',
                        formData: this.formDataEdit
                       },
                action: 'openPage',
                icon: 'faEdit'
              },
              {
                id: 2,
                text: 'Delete',
                class: 'btn btn-secondary',
                icon: 'faTrash'
              },
              {
                id: 3,
                text: 'Set to Inactive',
                class: 'btn btn-danger',
                icon: 'faBan'
              }
            ]
          },
          {
            type: 'status',
            class: 'btn btn-success',
            content: 'Active'
          },
          {
            type: 'avatar',
            content: 'https://th.bing.com/th/id/R.78f6c7e170c2aa3027e4d4d9e8a9bedc?rik=2EC%2fcZeaJSpFiw&riu=http%3a%2f%2fimages2.fanpop.com%2fimages%2fphotos%2f3000000%2fArshavin-wallpaper-andrei-arshavin-3031250-800-600.jpg&ehk=VMaTiyXuMCh5PHqCbFZt2zo8DCgHc3uW0jr3Vx2zbbE%3d&risl=&pid=ImgRaw&r=0',
            shape: 'circle',
            size: 'large'
          },
          {
            type: 'text',
            content: 'Banner 3'
          },
          {
            type: 'text',
            content: 'Province',
            code: 'province'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          }
        ]
      }
    ]
  }

  searchInputData = [
    {
      id: 1,
      label: 'Banner Name',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'Banner Name',
      value: ''
    },
    {
      id: 2,
      label: 'Banner Type',
      type: 'select-box',
      placeholder: 'Select Banner Type',
      value: '',
      options: [
        {
          code: 'all',
          name: 'All'
        },
        {
          code: 'country',
          name: 'Country'
        },
        {
          code: 'province',
          name: 'Province'
        },
        {
          code: 'city',
          name: 'City'
        }
      ]
    },
    {
      id: 3,
      label: 'Status',
      type: 'select-box',
      placeholder: 'Select Status',
      value: '',
      options: [
        {
          code: 'active',
          name: 'Active'
        },
        {
          code: 'inactive',
          name: 'Inactive'
        }
      ]
    }
   ]

}
