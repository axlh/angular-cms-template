import { Component } from '@angular/core';
import { TableComponent } from '../../components/table/table.component';
import { UtilityService } from '../../services/utility/utility.service';

@Component({
  selector: 'app-faq-categories',
  standalone: true,
  imports: [ TableComponent ],
  templateUrl: './faq-categories.page.html',
  styleUrl: './faq-categories.page.css'
})
export class FaqCategoriesPage {
  constructor(
    public utility: UtilityService
  ){
    
  }

  formData = {
    type: 'add',
    data: [
      {
        id: 1,
        label: 'FAQ Category Name',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter FAQ Category Name',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 2,
        label: 'Status',
        type: 'select-box',
        placeholder: 'Select Status',
        required: true,
        value: '',
        options: [
          {
            code: 'active',
            name: 'Active'
          },
          {
            code: 'inactive',
            name: 'Inactive'
          }
        ],
        disabled: false
      }
    ]
  }

  formDataEdit = {
    type: 'edit',
    data: [
      {
        id: 1,
        label: 'Status',
        type: 'select-box',
        placeholder: 'Select Status',
        required: false,
        value: '',
        options: [
          {
            code: 'active',
            name: 'Active'
          },
          {
            code: 'inactive',
            name: 'Inactive'
          }
        ],
        disabled: true,
        hide: true
      },
      {
        id: 2,
        label: 'FAQ Category Name',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter FAQ Category Name',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 3,
        label: 'Last Modified by',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Last Modified by',
        required: false,
        value: '',
        disabled: true,
        hide: true
      },
      {
        id: 4,
        label: 'Last Modified',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Last Modified',
        required: false,
        value: '',
        disabled: true,
        hide: true
      },
      {
        id: 5,
        label: 'Created by',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Created by',
        required: false,
        value: '',
        disabled: true,
        hide: true
      },
      {
        id: 6,
        label: 'Created Date',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Created Date',
        required: false,
        value: '',
        disabled: true,
        hide: true
      }
    ]
  }

  data = {
    head: [
      {
        text: '#'
      },
      {
        text: 'Action'
      },
      {
        text: 'Status'
      },
      {
        text: 'FAQ Category Name'
      },
      {
        text: 'Last Modified by',
      },
      {
        text: 'Last Modified',
      },
      {
        text: 'Created By',
      },
      {
        text: 'Created Date',
      }
    ],
    body: [
      {
        id: 1,
        data: [
          {
            type: 'buttons',
            buttons : [
              {
                id: 1,
                text: 'Edit',
                class: 'btn btn-primary',
                props: {
                        module: 'FAQ Categories', 
                        subModule: 'Edit FAQ Category',
                        title: 'Edit FAQ Category',
                        backRoute: '/faq-categories',
                        formData: this.formDataEdit
                       },
                action: 'openPage',
                icon: 'faEdit'
              },
              {
                id: 2,
                text: 'Set to Active',
                class: 'btn btn-success',
                icon: 'faCheck'
              }
            ]
          },
          {
            type: 'status',
            class: 'btn btn-danger',
            content: 'Inactive'
          },
          {
            type: 'text',
            content: 'New'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          }
        ]
      },
      {
        id: 2,
        data: [
          {
            type: 'buttons',
            buttons : [
              {
                id: 1,
                text: 'Edit',
                class: 'btn btn-primary',
                props: {
                        module: 'FAQ Categories', 
                        subModule: 'Edit FAQ Category',
                        title: 'Edit FAQ Category',
                        backRoute: '/faq-categories',
                        formData: this.formDataEdit
                       },
                action: 'openPage',
                icon: 'faEdit'
              },
              {
                id: 2,
                text: 'Set to Inactive',
                class: 'btn btn-danger',
                icon: 'faBan'
              }
            ]
          },
          {
            type: 'status',
            class: 'btn btn-success',
            content: 'Active'
          },
          {
            type: 'text',
            content: 'Frequently'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          }
        ]
      },
      {
        id: 3,
        data: [
          {
            type: 'buttons',
            buttons : [
              {
                id: 1,
                text: 'Edit',
                class: 'btn btn-primary',
                props: {
                        module: 'FAQ Categories', 
                        subModule: 'Edit FAQ Category',
                        title: 'Edit FAQ Category',
                        backRoute: '/faq-categories',
                        formData: this.formDataEdit
                       },
                action: 'openPage',
                icon: 'faEdit'
              },
              {
                id: 2,
                text: 'Set to Inactive',
                class: 'btn btn-danger',
                icon: 'faBan'
              }
            ]
          },
          {
            type: 'status',
            class: 'btn btn-success',
            content: 'Active'
          },
          {
            type: 'text',
            content: 'Price'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          }
        ]
      }
    ]
  }

  searchInputData = [
    {
      id: 1,
      label: 'FAQ Category Name',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'FAQ Category Name',
      value: ''
    },
    {
      id: 2,
      label: 'Status',
      type: 'select-box',
      placeholder: 'Select Status',
      value: '',
      options: [
        {
          code: 'active',
          name: 'Active'
        },
        {
          code: 'inactive',
          name: 'Inactive'
        }
      ]
    }
   ]
}
