import { Component } from '@angular/core';
import { ChartComponent } from '../../components/chart/chart.component';
import { ProductListComponent } from '../../components/product-list/product-list.component';
import { MapComponent } from '../../components/map/map.component';
import { PanelInfoComponent } from '../../components/panel-info/panel-info.component';
import { PanelInfoIconComponent } from '../../components/panel-info-icon/panel-info-icon.component';
import { Subscription } from 'rxjs';
import { UiService } from '../../services/ui/ui.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-content',
  standalone: true,
  imports: [ CommonModule, ChartComponent, ProductListComponent, MapComponent, PanelInfoComponent, PanelInfoIconComponent ],
  templateUrl: './dashboard.page.html',
  styleUrl: './dashboard.page.css'
})
export class DahsboardPage {
  showNavbar: boolean = true;
  subscription: Subscription | undefined;

  constructor(
    private uiService: UiService
  ){
    this.subscription = this.uiService
    .onToggleNavbar()
    .subscribe((value) => {
      this.showNavbar = value;
    });
  }

  salesData = {
    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
    datasets: [
      {
        label: 'Sales Amount',
        backgroundColor: '#32a852',
        borderColor: '#116613',
        data: [12, 19, 3, 5, 2, 3, 40, 20, 31, 11, 22, 12],
        borderWidth: 3,
      },
    ],
  }

  optionsSales =  {
    maintainAspectRatio: false,
    responsive: true,
    aspectRatio: 1,
    plugins: {
      legend: {
        display: false
      }
    }
  }

  topProvinceData = {
    labels: ['Jawa Barat', 'Jakarta', 'Lampung', 'Sulawesi Utara', 'Bali'],
    data: [19,19,20,10,11],
    datasets: [
      {
        label : 'Jawa Barat',
        backgroundColor: ['#5acc5a', '#5b5d63', '#ad39aa', '#c8db37', '#143cb5'],
        data: [12,18,20,10,11],
        borderWidth: 0,
      }
    ]
  }

  optionsProvince =  {
    maintainAspectRatio: false,
    responsive: true,
    aspectRatio: 1,
    plugins: {
      legend: {
        display: false,
        labels: {
          color: '#000'
        }
      }
    }
  }

  productData = [
    {
      id: 1,
      name: "GLacier Steal Jacket",
      rate: 5,
      total_sold: 283,
      material: "100% Cotton",
      weight: "Light weight",
      tagline: "Best finish",
      design_type: "Unique design",
      for: "For men",
      use_type: "Casual",
      img: "https://sam-nyc.com/cdn/shop/products/GLACIER_STEEL_019_600x.jpg?v=1634676604",
      description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint, dolore. Ipsum blanditiis, quo cumque praesentium commodi eligendi sit odit maiores corporis modi similique iste, esse ipsa culpa debitis eaque. Modi."
    },
    {
      id: 1,
      name: "Hight Wist Jeans",
      rate: 4,
      total_sold: 280,
      material: "100% jeans",
      weight: "Light weight",
      tagline: "Best finish",
      design_type: "Unique design",
      for: "For women",
      use_type: "Casual",
      img: "https://www.versace.com/dw/image/v2/BGWN_PRD/on/demandware.static/-/Sites-ver-master-catalog/default/dw9786da1c/original/90_E75HAB508-EDW023L54_E904_10_High~Waist~Jeans-Pants-Versace-online-store_1_0.jpg?sw=850&q=85&strip=true",
      description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint, dolore. Ipsum blanditiis, quo cumque praesentium commodi eligendi sit odit maiores corporis modi similique iste, esse ipsa culpa debitis eaque. Modi."
    },
  ]

  infoIconData = {
    data_name: 'Admin',
    total: 16,  
    icon: 'faUserTie',
    route:'/admin'
  }
}
