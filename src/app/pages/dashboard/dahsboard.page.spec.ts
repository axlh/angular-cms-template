import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DahsboardPage } from './dashboard.page';

describe('ContentComponent', () => {
  let component: DahsboardPage;
  let fixture: ComponentFixture<DahsboardPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DahsboardPage]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DahsboardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
