import { Component } from '@angular/core';
import { TableComponent } from '../../components/table/table.component';
import { UtilityService } from '../../services/utility/utility.service';

@Component({
  selector: 'app-order',
  standalone: true,
  imports: [ TableComponent ],
  templateUrl: './order.page.html',
  styleUrl: './order.page.css'
})
export class OrderPage {

  constructor(
    public utility: UtilityService
  ){}

  data = {
    head: [
      {
        text: '#'
      },
      {
        text: 'Action'
      },
      {
        text: 'Order Status'
      },
      {
        text: 'Order Number'
      },
      {
        text: 'Order Date'
      },
      {
        text: 'Custumer Name'
      },
      {
        text: 'Total Price'
      },
      {
        text: 'Last Modified by',
      },
      {
        text: 'Last Modified',
      },
      {
        text: 'Created By',
      },
      {
        text: 'Created Date',
      }
    ],
    body: [
      {
        id: 1,
        data: [
          {
            type: 'buttons',
            buttons: [
              {
                id: 1,
                text: 'Detail',
                class: 'btn btn-info text-light',
                routerLink: '/data-form',
                action: 'openPage',
                icon: 'faEye'
              }
            ]
          },
          {
            type: 'status',
            class: 'btn btn-warning',
            code: 'inprogress',
            content: 'In Progress'
          },
          {
            type: 'text',
            content: 'ORD-12022023'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          },
          {
            type: 'text',
            content: 'Kylian Mbappe'
          },
          {
            type: 'text',
            content: this.utility.setCurrency(BigInt(30))
          },
          {
            type: 'text',
            content: 'Kylian Mbappe'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          },
          {
            type: 'text',
            content: 'Kylian Mbappe'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          }
        ]
      },
      {
        id: 2,
        data: [
          {
            type: 'buttons',
            buttons: [
              {
                id: 1,
                text: 'Detail',
                class: 'btn btn-info text-light',
                action: 'openPage',
                icon: 'faEye'
              }
            ]
          },
          {
            type: 'status',
            class: 'btn btn-success',
            code: 'done',
            content: 'Done'
          },
          {
            type: 'text',
            content: 'ORD-12022023'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          },
          {
            type: 'text',
            content: 'Marcus Rashford'
          },
          {
            type: 'text',
            content: this.utility.setCurrency(BigInt(45))
          },
          {
            type: 'text',
            content: 'Marcus Rashford'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          },
          {
            type: 'text',
            content: 'Marcus Rashford'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          }
        ]
      }
    ]
  }

  formData = {
    type: 'detail',
    data: 
      [
        {
          id: 1,
          label: 'Order Status',
          type: 'select-box',
          options: [
            {
              code: 'new',
              name: 'New'
            },
            {
              code: 'inprogress',
              name: 'In Progress'
            },
            {
              code: 'done',
              name: 'Done'
            },
          ]
        },
        {
          id: 2,
          label: 'Order Number',
          type: 'input-text',
          inputType: 'text'
        },
        {
          id: 3,
          label: 'Order Date',
          type: 'input-text',
          inputType: 'text'
        },
        {
          id: 4,
          label: 'Custumer Name',
          type: 'input-text',
          inputType: 'text'
        },
        {
          id: 5,
          label: 'Total Price',
          type: 'input-text',
          inputType: 'text'
        },
        {
          id: 6,
          label: 'Last Modified by',
          type: 'input-text',
          inputType: 'text'
        },
        {
          id: 7,
          label: 'Last Modified',
          type: 'input-text',
          inputType: 'text'
        },
        {
          id: 8,
          label: 'Created By',
          type: 'input-text',
          inputType: 'text'
        },
        {
          id: 9,
          label: 'Created Date',
          type: 'input-text',
          inputType: 'text'
        }
      ]
    }

    searchInputData = [
      {
        id: 1,
        label: 'Order Number',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Order Number',
        value: ''
      },
      {
        id: 2,
        label: 'Order Date Range',
        type: 'date',
        inputType: 'date',
        placeholder: 'Order Date Range',
        value: ''
      },
      {
        id: 3,
        label: 'Customer Name',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Customer Name',
        value: ''
      },
      {
        id: 4,
        label: 'Total Price',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Total Price',
        value: ''
      },
      {
        id: 5,
        label: 'Status',
        type: 'select-box',
        placeholder: 'Select Status',
        value: '',
        options: [
          {
            code: 'new',
            name: 'New'
          },
          {
            code: 'inprogress',
            name: 'In Progress'
          },
          {
            code: 'done',
            name: 'Done'
          },
        ]
      }
     ]
}
