import { Component } from '@angular/core';
import { TableComponent } from '../../components/table/table.component';

@Component({
  selector: 'app-user',
  standalone: true,
  imports: [ TableComponent ],
  templateUrl: './user.page.html',
  styleUrl: './user.page.css'
})
export class UserPage {
  formData = {
    type: 'add',
    data: [
      {
        id: 1,
        label: 'Name',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Name',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 2,
        label: 'Email',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Email',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 3,
        label: 'Phone',
        type: 'input-text',
        inputType: 'phone',
        placeholder: 'Enter Phone Number',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 4,
        label: 'Age',
        type: 'input-text',
        inputType: 'number',
        placeholder: 'Enter Age',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 5,
        label: 'State',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter State',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 6,
        label: 'Province',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Province',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 7,
        label: 'City',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter City',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 8,
        label: 'Address',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Address',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 9,
        label: 'Password',
        type: 'input-text',
        inputType: 'password',
        placeholder: 'Enter Password',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 10,
        label: 'Gender',
        type: 'select-box',
        placeholder: 'Select Gender',
        required: true,
        value: '',
        options: [
          {
            code: 'male',
            name: 'Male'
          },
          {
            code: 'female',
            name: 'Female'
          }
        ],
        disabled: false
      },
      {
        id: 11,
        label: 'Profile Image',
        type: 'image',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 12,
        label: 'Status',
        type: 'select-box',
        placeholder: 'Select Status',
        required: true,
        value: '',
        options: [
          {
            code: 'active',
            name: 'Active'
          },
          {
            code: 'inactive',
            name: 'Inactive'
          }
        ],
        disabled: false
      }
    ]
  }

  formDataEdit = {
    type: 'edit',
    data: [
      {
        id: 1,
        label: 'Status',
        type: 'select-box',
        placeholder: 'Select Status',
        required: false,
        value: '',
        options: [
          {
            code: 'active',
            name: 'Active'
          },
          {
            code: 'inactive',
            name: 'Inactive'
          }
        ],
        disabled: true,
        hide: true
      },
      {
        id: 2,
        label: 'Name',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Name',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 3,
        label: 'Profile Image',
        type: 'image',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 4,
        label: 'Email',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Email',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 5,
        label: 'Phone',
        type: 'input-text',
        inputType: 'phone',
        placeholder: 'Enter Phone Number',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 6,
        label: 'Age',
        type: 'input-text',
        inputType: 'number',
        placeholder: 'Enter Age',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 7,
        label: 'State',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter State',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 8,
        label: 'Province',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Province',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 9,
        label: 'City',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter City',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 10,
        label: 'Address',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Address',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 11,
        label: 'Gender',
        type: 'select-box',
        placeholder: 'Select Gender',
        required: true,
        value: '',
        options: [
          {
            code: 'male',
            name: 'Male'
          },
          {
            code: 'female',
            name: 'Female'
          }
        ],
        disabled: false,
      },
      {
        id: 12,
        label: 'Last Modified by',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Last Modified by',
        required: false,
        value: '',
        disabled: true,
        hide: true
      },
      {
        id: 13,
        label: 'Last Modified',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Last Modified',
        required: false,
        value: '',
        disabled: true,
        hide: true
      },
      {
        id: 14,
        label: 'Created by',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Created by',
        required: false,
        value: '',
        disabled: true,
        hide: true
      },
      {
        id: 15,
        label: 'Created Date',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Created Date',
        required: false,
        value: '',
        disabled: true,
        hide: true
      }
    ]
  }

  data = {
    head: [
      {
        text:'#'
      },
      {
        text: 'Action'
      },
      {
        text: 'Status'
      },
      {
        text: 'Name'
      },
      {
        text: 'Image'
      },
      {
        text: 'Email'
      },
      {
        text: 'Phone'
      },
      {
        text: 'Age'
      },
      {
        text: 'State'
      },
      {
        text: 'Province'
      },
      {
        text: 'City'
      },
      {
        text: 'Address'
      },
      {
        text: 'Gender'
      },
      {
        text: 'Last Modified by',
      },
      {
        text: 'Last Modified',
      },
      {
        text: 'Created By',
      },
      {
        text: 'Created Date',
      }
    ],
    body: [
      {
        id: 1,
        data: [
          {
            type: 'buttons',
            buttons: [
              {
                id: 1,
                text: 'Edit',
                class: 'btn btn-primary',
                props: {
                  module: 'Admin', 
                  subModule: 'Edit Administrator',
                  title: 'Edit Administrator',
                  backRoute: '/user',
                  formData: this.formDataEdit
                 },
                action: 'openPage',
                icon: 'faEdit'
              },
              {
                id: 2,
                text: 'Delete',
                class: 'btn btn-secondary',
                icon: 'faTrash'
              },
              {
                id: 2,
                text: 'Set to Inactive',
                class: 'btn btn-danger',
                icon: 'faBan'
              }
            ]
          },
          {
            type: 'status',
            class: 'btn btn-success',
            content: 'Active',
            code: 'active'
          },
          {
            type: 'text',
            content: 'Bruno Marz'
          },
          {
            type: 'avatar',
            content: 'https://wallpapercave.com/wp/wp2094469.jpg',
            shape: 'circle',
            size: 'large'
          },
          {
            type: 'text',
            content: 'bruno@gmail.com'
          },
          {
            type: 'text',
            content: '0812121212'
          },
          {
            type: 'text',
            content: '38'
          },
          {
            type: 'text',
            content: 'United State of America'
          },
          {
            type: 'text',
            content: 'California'
          },
          {
            type: 'text',
            content: 'Los Santos'
          },{
            type: 'text',
            content: 'Jl. Besterpost RW 05/ RT 01 No.115'
          },
          {
            type: 'text',
            content: 'Male',
            code: 'male'
          },
          {
            type: 'text',
            content: 'Bruno Marz'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: `2022-12-06 16:53:02`
          }
        ]
      },
      {
       id: 2,
       data: [
        {
          type: 'buttons',
          buttons: [
            {
              id: 1,
              text: 'Edit',
              class: 'btn btn-primary',
              props: {
                module: 'Admin', 
                subModule: 'Edit Administrator',
                title: 'Edit Administrator',
                backRoute: '/user',
                formData: this.formDataEdit
               },
              action: 'openPage',
              icon: 'faEdit'
            },
            {
              id: 2,
              text: 'Delete',
              class: 'btn btn-secondary',
              icon: 'faTrash'
            },
            {
              id: 3,
              text: 'Set to Inactive',
              class: 'btn btn-danger',
              icon: 'faBan'
            }
          ]
        },
        {
          type: 'status',
          class: 'btn btn-success',
          content: 'Active'
        },
        {
          type: 'text',
          content: 'Mucha Luca'
        },
        {
          type: 'avatar',
          content: 'https://th.bing.com/th/id/OIP.Z5mrdtfIr5VUWHyW4MBengHaEK?rs=1&pid=ImgDetMain',
          shape: 'circle',
          size: 'large'
        },
        {
          type: 'text',
          content: 'Mucha@lucha.com'
        },
        {
          type: 'text',
          content: '0842222329902'
        },
        {
          type: 'text',
          content: '48'
        },
        {
          type: 'text',
          content: 'Puertorico',
        },
        {
          type: 'text',
          content: 'El Savalas'
        },
        {
          type: 'text',
          content: 'Las Venturas'
        },
        {
          type: 'text',
          content: 'Jl. Cantiga RT 15/ RW 3 No. 63'
        },
        {
          type: 'text',
          content: 'Male',
          code: 'male'
        },
        {
          type: 'text',
          content: 'Mucha Luca'
        },
        {
          type: 'text',
          content: '2024-02-10 19:00:00'
        },
        {
          type: 'text',
          content: 'Mucha Luca'
        },
        {
          type: 'text',
          content: '2024-02-09 19:32:44'
        }
       ] 
      }
    ]
  }

  searchInputData = [
    {
      id: 1,
      label: 'Name',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'Name',
      value: ''
    },
    {
      id: 2,
      label: 'Email',
      type: 'input-text',
      inputType: 'email',
      placeholder: 'Email',
      value: ''
    },
    {
      id: 3,
      label: 'Phone',
      type: 'input-text',
      inputType: 'phone',
      placeholder: 'Phone',
      value: ''
    },
    {
      id: 4,
      label: 'Age',
      type: 'input-text',
      inputType: 'number',
      placeholder: 'Age',
      value: ''
    },
    {
      id: 5,
      label: 'State',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'State',
      value: ''
    },
    {
      id: 6,
      label: 'Provice',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'Provice',
      value: ''
    },
    {
      id: 7,
      label: 'City',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'City',
      value: ''
    },
    {
      id: 8,
      label: 'Address',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'Address',
      value: ''
    },
    {
      id: 9,
      label: 'Gender',
      type: 'select-box',
      placeholder: 'Select Gender',
      value: '',
      options: [
        {
          code: 'male',
          name: 'Male'
        },
        {
          code: 'female',
          name: 'Female'
        }
      ]
    },
    {
      id: 5,
      label: 'Status',
      type: 'select-box',
      placeholder: 'Select Status',
      value: '',
      options: [
        {
          code: 'active',
          name: 'Active'
        },
        {
          code: 'inactive',
          name: 'Inactive'
        }
      ]
    }
   ]
}
