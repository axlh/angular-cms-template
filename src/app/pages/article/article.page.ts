import { Component } from '@angular/core';
import { UtilityService } from '../../services/utility/utility.service';
import { TableComponent } from '../../components/table/table.component';

@Component({
  selector: 'app-article',
  standalone: true,
  imports: [ TableComponent ],
  templateUrl: './article.page.html',
  styleUrl: './article.page.css'
})
export class ArticlePage {
  constructor(
    public utility: UtilityService
  ){

  }

  formData = {
    type: 'add',
    data: [
      {
        id: 1,
        label: 'Title',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Title',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 2,
        label: 'Content',
        type: 'article',
        inputType: 'text',
        placeholder: 'Write your article here...',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 3,
        label: 'Image',
        type: 'image',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 4,
        label: 'Status',
        type: 'select-box',
        placeholder: 'Select Status',
        required: true,
        value: '',
        options: [
          {
            code: 'active',
            name: 'Active'
          },
          {
            code: 'inactive',
            name: 'Inactive'
          }
        ],
        disabled: false
      }
    ]
  }

  formDataEdit = {
    type: 'edit',
    data: [
      {
        id: 1,
        label: 'Status',
        type: 'select-box',
        placeholder: 'Select Status',
        required: false,
        value: '',
        options: [
          {
            code: 'active',
            name: 'Active'
          },
          {
            code: 'inactive',
            name: 'Inactive'
          }
        ],
        disabled: true,
        hide: true
      },
      {
        id: 2,
        label: 'Image',
        type: 'image',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 3,
        label: 'Title',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Title',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 4,
        label: 'Content',
        type: 'article',
        inputType: 'text',
        placeholder: 'Write your article here...',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 5,
        label: 'Last Modified by',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Last Modified by',
        required: false,
        value: '',
        disabled: true,
        hide: true
      },
      {
        id: 6,
        label: 'Last Modified',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Last Modified',
        required: false,
        value: '',
        disabled: true,
        hide: true
      },
      {
        id: 7,
        label: 'Created by',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Created by',
        required: false,
        value: '',
        disabled: true,
        hide: true
      },
      {
        id: 8,
        label: 'Created Date',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Created Date',
        required: false,
        value: '',
        disabled: true,
        hide: true
      }
    ]
  }

  data = {
    head: [
      {
        text: '#'
      },
      {
        text: 'Action'
      },
      {
        text: 'Status'
      },
      {
        text: 'Image'
      },
      {
        text: 'Title'
      },
      {
        text: 'Content'
      },
      {
        text: 'Last Modified by',
      },
      {
        text: 'Last Modified',
      },
      {
        text: 'Created By',
      },
      {
        text: 'Created Date',
      }
    ],
    body: [
      {
        id: 1,
        data: [
          {
            type: 'buttons',
            buttons: [
              {
                id: 1,
                text: 'Edit',
                class: 'btn btn-primary',
                props: {
                  module: 'Article', 
                  subModule: 'Edit Article',
                  title: 'Edit Article',
                  backRoute: '/article',
                  formData: this.formDataEdit
                 },
                action: 'openPage',
                icon: 'faEdit'
              },
              {
                id: 2,
                text: 'Set to Inactive',
                class: 'btn btn-danger',
                icon: 'faBan'
              }
            ]
          },
          {
            type: 'status',
            class: 'btn btn-success',
            content: 'Active'
          },
          {
            type: 'avatar',
            content: 'https://th.bing.com/th/id/R.78f6c7e170c2aa3027e4d4d9e8a9bedc?rik=2EC%2fcZeaJSpFiw&riu=http%3a%2f%2fimages2.fanpop.com%2fimages%2fphotos%2f3000000%2fArshavin-wallpaper-andrei-arshavin-3031250-800-600.jpg&ehk=VMaTiyXuMCh5PHqCbFZt2zo8DCgHc3uW0jr3Vx2zbbE%3d&risl=&pid=ImgRaw&r=0',
            shape: 'circle',
            size: 'large'
          },
          {
            type: 'text',
            content: 'What is Lorem Ipsum?',
            sort: 30
          },
          {
            type: 'innerContent',
            content: "<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>",
            sort: 60
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          }
        ]
      },
      {
        id: 2,
        data: [
          {
            type: 'buttons',
            buttons: [
              {
                id: 1,
                text: 'Edit',
                class: 'btn btn-primary',
                props: {
                  module: 'Article', 
                  subModule: 'Edit Article',
                  title: 'Edit Article',
                  backRoute: '/article',
                  formData: this.formDataEdit
                 },
                action: 'openPage',
                icon: 'faEdit'
              },
              {
                id: 2,
                text: 'Set to Inactive',
                class: 'btn btn-danger',
                icon: 'faBan'
              }
            ]
          },
          {
            type: 'status',
            class: 'btn btn-success',
            content: 'Active'
          },
          {
            type: 'avatar',
            content: 'https://th.bing.com/th/id/R.78f6c7e170c2aa3027e4d4d9e8a9bedc?rik=2EC%2fcZeaJSpFiw&riu=http%3a%2f%2fimages2.fanpop.com%2fimages%2fphotos%2f3000000%2fArshavin-wallpaper-andrei-arshavin-3031250-800-600.jpg&ehk=VMaTiyXuMCh5PHqCbFZt2zo8DCgHc3uW0jr3Vx2zbbE%3d&risl=&pid=ImgRaw&r=0',
            shape: 'circle',
            size: 'large'
          },
          {
            type: 'text',
            content: 'Why do we use it?',
            sort: 30
          },
          {
            type: 'text',
            content: "<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>",
            sort: 60
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          }
        ]
      },
      {
        id: 3,
        data: [
          {
            type: 'buttons',
            buttons: [
              {
                id: 1,
                text: 'Edit',
                class: 'btn btn-primary',
                props: {
                  module: 'Article', 
                  subModule: 'Edit Article',
                  title: 'Edit Article',
                  backRoute: '/article',
                  formData: this.formDataEdit
                 },
                action: 'openPage',
                icon: 'faEdit'
              },
              {
                id: 2,
                text: 'Set to Inactive',
                class: 'btn btn-danger',
                icon: 'faBan'
              }
            ]
          },
          {
            type: 'status',
            class: 'btn btn-success',
            content: 'Active'
          },
          {
            type: 'avatar',
            content: 'https://th.bing.com/th/id/R.78f6c7e170c2aa3027e4d4d9e8a9bedc?rik=2EC%2fcZeaJSpFiw&riu=http%3a%2f%2fimages2.fanpop.com%2fimages%2fphotos%2f3000000%2fArshavin-wallpaper-andrei-arshavin-3031250-800-600.jpg&ehk=VMaTiyXuMCh5PHqCbFZt2zo8DCgHc3uW0jr3Vx2zbbE%3d&risl=&pid=ImgRaw&r=0',
            shape: 'circle',
            size: 'large'
          },
          {
            type: 'text',
            content: 'Where does it come from?',
            sort: 30
          },
          {
            type: 'text',
            content: '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p><p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>',
            sort: 60
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          }
        ]
      }
    ]
  }

  searchInputData = [
    {
      id: 1,
      label: 'Title',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'Title',
      value: ''
    },
    {
      id: 2,
      label: 'Status',
      type: 'select-box',
      placeholder: 'Select Status',
      value: '',
      options: [
        {
          code: 'active',
          name: 'Active'
        },
        {
          code: 'inactive',
          name: 'Inactive'
        }
      ]
    }
   ]
}
