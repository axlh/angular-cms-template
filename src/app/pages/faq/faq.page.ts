import { Component } from '@angular/core';
import { UtilityService } from '../../services/utility/utility.service';
import { TableComponent } from '../../components/table/table.component';

@Component({
  selector: 'app-faq',
  standalone: true,
  imports: [ TableComponent ],
  templateUrl: './faq.page.html',
  styleUrl: './faq.page.css'
})
export class FaqPage {
  constructor(
    public utility: UtilityService
  ){
    
  }

  formData = {
    type: 'add',
    data: [
      {
        id: 1,
        label: 'FAQ Title',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter FAQ Title',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 2,
        label: 'FAQ Category',
        type: 'select-box',
        placeholder: 'Select FAQ Category',
        required: true,
        value: '',
        options: [
          {
            code: 'new',
            name: 'New'
          },
          {
            code: 'frequently',
            name: 'Frequently'
          },
          {
            code: 'price',
            name: 'Price'
          }
        ],
        disabled: false
      },
      {
        id: 3,
        label: 'Status',
        type: 'select-box',
        placeholder: 'Select Status',
        required: true,
        value: '',
        options: [
          {
            code: 'active',
            name: 'Active'
          },
          {
            code: 'inactive',
            name: 'Inactive'
          }
        ],
        disabled: false
      }
    ]
  }

  formDataEdit = {
    type: 'edit',
    data: [
      {
        id: 1,
        label: 'Status',
        type: 'select-box',
        placeholder: 'Select Status',
        required: false,
        value: '',
        options: [
          {
            code: 'active',
            name: 'Active'
          },
          {
            code: 'inactive',
            name: 'Inactive'
          }
        ],
        disabled: true,
        hide: true
      },
      {
        id: 2,
        label: 'FAQ Category',
        type: 'select-box',
        placeholder: 'Select FAQ Category',
        required: false,
        value: '',
        options: [
          {
            code: 'new',
            name: 'New'
          },
          {
            code: 'frequently',
            name: 'Frequently'
          },
          {
            code: 'price',
            name: 'Price'
          }
        ],
        disabled: false
      },
      {
        id: 3,
        label: 'FAQ Title',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter FAQ Title',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 4,
        label: 'Last Modified by',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Last Modified by',
        required: false,
        value: '',
        disabled: true,
        hide: true
      },
      {
        id: 5,
        label: 'Last Modified',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Last Modified',
        required: false,
        value: '',
        disabled: true,
        hide: true
      },
      {
        id: 6,
        label: 'Created by',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Created by',
        required: false,
        value: '',
        disabled: true,
        hide: true
      },
      {
        id: 7,
        label: 'Created Date',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Created Date',
        required: false,
        value: '',
        disabled: true,
        hide: true
      }
    ]
  }

  data = {
    head: [
      {
        text: '#'
      },
      {
        text: 'Action'
      },
      {
        text: 'Status'
      },
      {
        text: 'FAQ Category Name'
      },
      {
        text: 'FAQ Title'
      },
      {
        text: 'Last Modified by',
      },
      {
        text: 'Last Modified',
      },
      {
        text: 'Created By',
      },
      {
        text: 'Created Date',
      }
    ],
    body: [
      {
        id: 1,
        data: [
          {
            type: 'buttons',
            buttons : [
              {
                id: 1,
                text: 'Edit',
                class: 'btn btn-primary',
                props: {
                        module: 'FAQ', 
                        subModule: 'Edit FAQ',
                        title: 'Edit FAQ',
                        backRoute: '/faq',
                        formData: this.formDataEdit
                       },
                action: 'openPage',
                icon: 'faEdit'
              },
              {
                id: 2,
                text: 'Set to Active',
                class: 'btn btn-success',
                icon: 'faCheck'
              }
            ]
          },
          {
            type: 'status',
            class: 'btn btn-danger',
            content: 'Inactive'
          },
          {
            type: 'text',
            content: 'New',
            code: 'new'
          },
          {
            type: 'text',
            content: 'When the App released?'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          }
        ]
      },
      {
        id: 2,
        data: [
          {
            type: 'buttons',
            buttons : [
              {
                id: 1,
                text: 'Edit',
                class: 'btn btn-primary',
                props: {
                        module: 'FAQ', 
                        subModule: 'Edit FAQ',
                        title: 'Edit FAQ',
                        backRoute: '/faq',
                        formData: this.formDataEdit
                       },
                action: 'openPage',
                icon: 'faEdit'
              },
              {
                id: 2,
                text: 'Set to Inactive',
                class: 'btn btn-danger',
                icon: 'faBan'
              }
            ]
          },
          {
            type: 'status',
            class: 'btn btn-success',
            content: 'Active'
          },
          {
            type: 'text',
            content: 'Frequently',
            code: 'frequently'
          },
          {
            type: 'text',
            content: 'Does App is free?'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          }
        ]
      },
      {
        id: 3,
        data: [
          {
            type: 'buttons',
            buttons : [
              {
                id: 1,
                text: 'Edit',
                class: 'btn btn-primary',
                props: {
                        module: 'FAQ', 
                        subModule: 'Edit FAQ',
                        title: 'Edit FAQ',
                        backRoute: '/faq',
                        formData: this.formDataEdit
                       },
                action: 'openPage',
                icon: 'faEdit'
              },
              {
                id: 2,
                text: 'Set to Inactive',
                class: 'btn btn-danger',
                icon: 'faBan'
              }
            ]
          },
          {
            type: 'status',
            class: 'btn btn-success',
            content: 'Active'
          },
          {
            type: 'text',
            content: 'Price',
            code: 'price'
          },
          {
            type: 'text',
            content: 'Does App take commissions ?'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          }
        ]
      }
    ]
  }

  searchInputData = [
    {
      id: 1,
      label: 'FAQ Title',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'FAQ Title',
      value: ''
    },
    {
      id: 2,
      label: 'FAQ Category',
      type: 'select-box',
      placeholder: 'Select FAQ Category',
      value: '',
      options: [
        {
          code: 'new',
          name: 'New'
        },
        {
          code: 'frequently',
          name: 'Frequently'
        },
        {
          code: 'price',
          name: 'Price'
        }
      ]
    },
    {
      id: 3,
      label: 'Status',
      type: 'select-box',
      placeholder: 'Select Status',
      value: '',
      options: [
        {
          code: 'active',
          name: 'Active'
        },
        {
          code: 'inactive',
          name: 'Inactive'
        }
      ]
    }
   ]
}
