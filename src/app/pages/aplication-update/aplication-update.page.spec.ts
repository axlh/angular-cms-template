import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AplicationUpdatePage } from './aplication-update.page';

describe('AplicationUpdatePage', () => {
  let component: AplicationUpdatePage;
  let fixture: ComponentFixture<AplicationUpdatePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AplicationUpdatePage]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AplicationUpdatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
