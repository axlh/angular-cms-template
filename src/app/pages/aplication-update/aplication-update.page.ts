import { Component } from '@angular/core';
import { UtilityService } from '../../services/utility/utility.service';
import { TableComponent } from '../../components/table/table.component';

@Component({
  selector: 'app-aplication-update',
  standalone: true,
  imports: [ TableComponent ],
  templateUrl: './aplication-update.page.html',
  styleUrl: './aplication-update.page.css'
})
export class AplicationUpdatePage {
  constructor(
    public utility: UtilityService
  ){}

  formData = {
    type: 'add',
    data: [
      {
        id: 1,
        label: 'Platform Type',
        type: 'select-box',
        placeholder: 'Select Platform Type',
        required: true,
        value: '',
        options: [
          {
            code: 'android',
            name: 'Android'
          },
          {
            code: 'ios',
            name: 'iOS'
          }
        ],
        disabled: false
      },
      {
        id: 2,
        label: 'Version',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Version',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 3,
        label: 'Build Number',
        type: 'input-text',
        inputType: 'number',
        placeholder: 'Enter Build Number',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 4,
        label: 'Notes',
        type: 'input-textarea',
        inputType: 'text',
        placeholder: 'Enter Notes',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 5,
        label: 'App Source URL',
        type: 'input-textarea',
        inputType: 'text',
        placeholder: 'Enter App Source URL',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 6,
        label: 'Status Active',
        type: 'select-box',
        placeholder: 'Select Status Active',
        required: true,
        value: '',
        options: [
          {
            code: 'yes',
            name: 'Yes'
          },
          {
            code: 'no',
            name: 'No'
          }
        ],
        disabled: false
      },
      {
        id: 7,
        label: 'Can Skip',
        type: 'select-box',
        placeholder: 'Select Can Skip',
        required: true,
        value: '',
        options: [
          {
            code: 'yes',
            name: 'Yes'
          },
          {
            code: 'no',
            name: 'No'
          }
        ],
        disabled: false
      }
    ]
  }

  formDataEdit = {
    type: 'edit',
    data: [
      {
        id: 1,
        label: 'Platform Type',
        type: 'select-box',
        placeholder: 'Select Platform Type',
        required: false,
        value: '',
        options: [
          {
            code: 'android',
            name: 'Android'
          },
          {
            code: 'ios',
            name: 'iOS'
          }
        ],
        disabled: false,
      },
      {
        id: 2,
        label: 'Version',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Version',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 3,
        label: 'Build Number',
        type: 'input-text',
        inputType: 'number',
        placeholder: 'Enter Build Number',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 4,
        label: 'Notes',
        type: 'input-textarea',
        inputType: 'text',
        placeholder: 'Enter Notes',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 5,
        label: 'App Source URL',
        type: 'input-textarea',
        inputType: 'text',
        placeholder: 'Enter App Source URL',
        required: true,
        value: '',
        disabled: false
      },
      {
        id: 6,
        label: 'Status Active',
        type: 'select-box',
        placeholder: 'Select Status Active',
        required: false,
        value: '',
        options: [
          {
            code: 'yes',
            name: 'Yes'
          },
          {
            code: 'no',
            name: 'No'
          }
        ],
        disabled: false,
      },
      {
        id: 7,
        label: 'Can Skip',
        type: 'select-box',
        placeholder: 'Select Can Skip',
        required: false,
        value: '',
        options: [
          {
            code: 'yes',
            name: 'Yes'
          },
          {
            code: 'no',
            name: 'No'
          }
        ],
        disabled: false,
      },
      {
        id: 8,
        label: 'Last Modified by',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Last Modified by',
        required: false,
        value: '',
        disabled: true,
        hide: true
      },
      {
        id: 9,
        label: 'Last Modified',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Last Modified',
        required: false,
        value: '',
        disabled: true,
        hide: true
      },
      {
        id: 10,
        label: 'Created by',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Created by',
        required: false,
        value: '',
        disabled: true,
        hide: true
      },
      {
        id: 11,
        label: 'Created Date',
        type: 'input-text',
        inputType: 'text',
        placeholder: 'Enter Created Date',
        required: false,
        value: '',
        disabled: true,
        hide: true
      }
    ]
  }

  data = {
    head: [
      {
        text: '#'
      },
      {
        text: 'Action'
      },
      {
        text: 'Platform Type'
      },
      {
        text: 'Version'
      },
      {
        text: 'Build'
      },
      {
        text: 'Notes'
      },
      {
        text: 'App Source URL'
      },
      {
        text: 'Active'
      },
      {
        text: 'Skip'
      },
      {
        text: 'Last Modified by',
      },
      {
        text: 'Last Modified',
      },
      {
        text: 'Created By',
      },
      {
        text: 'Created Date',
      }
    ],
    body: [
      {
        id: 1,
        data: [
          {
            type: 'buttons',
            buttons: [
              {
                id: 1,
                text: 'Edit',
                class: 'btn btn-primary',
                props: {
                        module: 'Aplication Update', 
                        subModule: 'Edit Aplication Update',
                        title: 'Edit Aplication Update',
                        backRoute: '/aplication',
                        formData: this.formDataEdit
                       },
                action: 'openPage',
                icon: 'faEdit'
              },
              {
                id: 2,
                text: 'Delete',
                class: 'btn btn-secondary',
                icon: 'faTrash'
              }
            ]
          },
          {
            type: 'platform',
            class: 'd-flex justify-content-center btn btn-success',
            content: 'Android',
            code: 'android',
            icon: 'faMobileAndroidAlt'
          },
          {
            type: 'text',
            content: '0.0.1'
          },
          {
            type: 'text',
            content: '1'
          },
          {
            type: 'text',
            content: '- Init App'
          },
          {
            type: 'text',
            content: 'https://drive.google.com/file/d/1JytZjkFsokskrrX0GO9KQiGk5fHvK6Fn/view?usp=drive_link'
          },
          {
            type: 'status',
            class: 'btn btn-success',
            content: 'Yes',
            code: 'yes'
          },
          {
            type: 'status',
            class: 'btn btn-success',
            content: 'Yes',
            code: 'yes'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          }
        ]
      },
      {
        id: 2,
        data: [
          {
            type: 'buttons',
            buttons: [
              {
                id: 1,
                text: 'Edit',
                class: 'btn btn-primary',
                props: {
                        module: 'Aplication Update', 
                        subModule: 'Edit Aplication Update',
                        title: 'Edit Aplication Update',
                        backRoute: '/aplication',
                        formData: this.formDataEdit
                       },
                action: 'openPage',
                icon: 'faEdit'
              },
              {
                id: 2,
                text: 'Delete',
                class: 'btn btn-secondary',
                icon: 'faTrash'
              }
            ]
          },
          {
            type: 'platform',
            class: 'd-flex justify-content-center btn btn-success',
            icon: 'faMobileAndroidAlt',
            content: 'Android',
            code: 'android'
          },
          {
            type: 'text',
            content: '0.0.2'
          },
          {
            type: 'text',
            content: '2'
          },
          {
            type: 'text',
            content: '- Bug fix login'
          },
          {
            type: 'text',
            content: 'https://drive.google.com/file/d/1JytZjkFsokskrrX0GO9KQiGk5fHvK6Fn/view?usp=drive_link'
          },
          {
            type: 'status',
            class: 'btn btn-success',
            content: 'Yes',
            code: 'yes'
          },
          {
            type: 'status',
            class: 'btn btn-danger',
            content: 'No',
            code: 'no'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          }
        ]
      },
      {
        id: 3,
        data: [
          {
            type: 'buttons',
            buttons: [
              {
                id: 1,
                text: 'Edit',
                class: 'btn btn-primary',
                props: {
                        module: 'Aplication Update', 
                        subModule: 'Edit Aplication Update',
                        title: 'Edit Aplication Update',
                        backRoute: '/aplication',
                        formData: this.formDataEdit
                       },
                action: 'openPage',
                icon: 'faEdit'
              },
              {
                id: 2,
                text: 'Delete',
                class: 'btn btn-secondary',
                icon: 'faTrash'
              }
            ]
          },
          {
            type: 'platform',
            class: 'd-flex justify-content-center btn btn-dark',
            icon: 'faAppleAlt',
            content: 'iOS',
            code: 'ios'
          },
          {
            type: 'text',
            content: '0.0.2'
          },
          {
            type: 'text',
            content: '2'
          },
          {
            type: 'text',
            content: '- Bug fix login'
          },
          {
            type: 'text',
            content: 'https://drive.google.com/file/d/1JytZjkFsokskrrX0GO9KQiGk5fHvK6Fn/view?usp=drive_link'
          },
          {
            type: 'status',
            class: 'btn btn-danger',
            content: 'No',
            code: 'no'
          },
          {
            type: 'status',
            class: 'btn btn-danger',
            content: 'No',
            code: 'no'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          },
          {
            type: 'text',
            content: 'Super Admin'
          },
          {
            type: 'text',
            content: '2022-12-06 16:53:02'
          }
        ]
      }
    ]
  }

  searchInputData = [
    {
      id: 1,
      label: 'Platform Type',
      type: 'select-box',
      placeholder: 'Select Platform Type',
      value: '',
      options: [
        {
          code: 'android',
          name: 'Android'
        },
        {
          code: 'ios',
          name: 'iOS'
        }
      ]
    },
    {
      id: 2,
      label: 'Version',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'Version',
      value: ''
    },
    {
      id: 3,
      label: 'Build',
      type: 'input-text',
      inputType: 'number',
      placeholder: 'Build',
      value: ''
    },
    {
      id: 4,
      label: 'Status Active',
      type: 'select-box',
      placeholder: 'Select Status Active',
      value: '',
      options: [
        {
          code: 'yes',
          name: 'Yes'
        },
        {
          code: 'no',
          name: 'No'
        }
      ]
    },
    {
      id: 5,
      label: 'Can Skip',
      type: 'select-box',
      placeholder: 'Select Can Skip',
      value: '',
      options: [
        {
          code: 'yes',
          name: 'Yes'
        },
        {
          code: 'no',
          name: 'No'
        }
      ]
    }
   ]
}
