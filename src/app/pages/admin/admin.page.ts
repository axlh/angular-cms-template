import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { TableComponent } from '../../components/table/table.component';
import { UtilityService } from '../../services/utility/utility.service';

@Component({
  selector: 'app-admin',
  standalone: true,
  imports: [ CommonModule, TableComponent ],
  templateUrl: './admin.page.html',
  styleUrl: './admin.page.css'
})
export class AdminPage {
 
 constructor(
  public utility: UtilityService
 ){

 }

 formData = {
  type: 'add',
  data: [
    {
      id: 1,
      label: 'Name',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'Enter Name',
      required: true,
      value: '',
      disabled: false
    },
    {
      id: 2,
      label: 'Email',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'Enter Email',
      required: true,
      value: '',
      disabled: false
    },
    {
      id: 3,
      label: 'Phone',
      type: 'input-text',
      inputType: 'phone',
      placeholder: 'Enter Phone Number',
      required: true,
      value: '',
      disabled: false
    },
    {
      id: 4,
      label: 'Password',
      type: 'input-text',
      inputType: 'password',
      placeholder: 'Enter Password',
      required: true,
      value: '',
      disabled: false
    },
    {
      id: 5,
      label: 'Profile Image',
      type: 'image',
      required: true,
      value: '',
      disabled: false
    },
    {
      id: 6,
      label: 'Status',
      type: 'select-box',
      placeholder: 'Select Status',
      required: true,
      value: '',
      options: [
        {
          code: 'active',
          name: 'Active'
        },
        {
          code: 'inactive',
          name: 'Inactive'
        }
      ],
      disabled: false
    }
  ]
}

formDataEdit = {
  type: 'edit',
  data: [
    {
      id: 1,
      label: 'Status',
      type: 'select-box',
      placeholder: 'Select Status',
      required: false,
      value: '',
      options: [
        {
          code: 'active',
          name: 'Active'
        },
        {
          code: 'inactive',
          name: 'Inactive'
        }
      ],
      disabled: true,
      hide: true
    },
    {
      id: 2,
      label: 'Name',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'Enter Name',
      required: true,
      value: '',
      disabled: false
    },
    {
      id: 3,
      label: 'Profile Image',
      type: 'image',
      required: true,
      value: '',
      disabled: false
    },
    {
      id: 4,
      label: 'Email',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'Enter Email',
      required: true,
      value: '',
      disabled: false
    },
    {
      id: 5,
      label: 'Phone',
      type: 'input-text',
      inputType: 'phone',
      placeholder: 'Enter Phone Number',
      required: true,
      value: '',
      disabled: false
    },
    {
      id: 6,
      label: 'Last Modified by',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'Enter Last Modified by',
      required: false,
      value: '',
      disabled: true,
      hide: true
    },
    {
      id: 7,
      label: 'Last Modified',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'Enter Last Modified',
      required: false,
      value: '',
      disabled: true,
      hide: true
    },
    {
      id: 8,
      label: 'Created by',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'Enter Created by',
      required: false,
      value: '',
      disabled: true,
      hide: true
    },
    {
      id: 9,
      label: 'Created Date',
      type: 'input-text',
      inputType: 'text',
      placeholder: 'Enter Created Date',
      required: false,
      value: '',
      disabled: true,
      hide: true
    }
  ]
}

 data = {
  head: 
    [
      {
        text: '#',
      },
      {
        text: 'Action',
      },
      {
        text: 'Status',
      },
      {
        text: 'Name'
      },
      {
        text: 'Image',
      },
      {
        text: 'Email',
      },
      {
        text: 'Phone',
      },
      {
        text: 'Last Modified by',
      },
      {
        text: 'Last Modified',
      },
      {
        text: 'Created By',
      },
      {
        text: 'Created Date',
      },
    ],
    body: [
      {
        id: 1,
        data: [
            {
              type: 'buttons',
              buttons : [
                {
                  id: 1,
                  text: 'Edit',
                  class: 'btn btn-primary',
                  props: {
                          module: 'Admin', 
                          subModule: 'Edit Administrator',
                          title: 'Edit Administrator',
                          backRoute: '/admin',
                          formData: this.formDataEdit
                         },
                  action: 'openPage',
                  icon: 'faEdit'
                },
                {
                  id: 2,
                  text: 'Set to Inactive',
                  class: 'btn btn-danger',
                  icon: 'faBan'
                }
              ]
            },
            {
              type: 'status',
              class: 'btn btn-success',
              code: 'active',
              content: 'Active'              
            },
            {
              type: 'text',
              content: 'Developer'
            },
            {
              type: 'avatar',
              content: 'https://th.bing.com/th/id/R.78f6c7e170c2aa3027e4d4d9e8a9bedc?rik=2EC%2fcZeaJSpFiw&riu=http%3a%2f%2fimages2.fanpop.com%2fimages%2fphotos%2f3000000%2fArshavin-wallpaper-andrei-arshavin-3031250-800-600.jpg&ehk=VMaTiyXuMCh5PHqCbFZt2zo8DCgHc3uW0jr3Vx2zbbE%3d&risl=&pid=ImgRaw&r=0',
              shape: 'circle',
              size: 'large'
            },
            {
              type: 'text',
              content: `Developer@gmail.com`,
            },
            {
              type: 'text',
              content: '081245999683'
            },
            {
              type: 'text',
              content: `Developer`

            },
            {
              type: 'text',
              content: '2022-12-06 16:53:02'
            },
            {
              type: 'text',
              content: `Developer`
            },
            {
              type: 'text',
              content: `2022-12-06 16:53:02`
            }
          ]
      },
      {
        id: 2,
        data: [
            {
              type: 'buttons',
              buttons : [
                {
                  id: 1,
                  text: 'Edit',
                  class: 'btn btn-primary',
                  props: {
                          module: 'Admin', 
                          subModule: 'Edit Administrator',
                          title: 'Edit Administrator',
                          backRoute: '/admin',
                          formData: this.formDataEdit
                         },
                  action: 'openPage',
                  icon: 'faEdit'
                },
                {
                  id: 2,
                  text: 'Set to Active',
                  class: 'btn btn-success',
                  icon: 'faCheck'
                }
              ]
            },
            {
              type: 'status',
              class: 'btn btn-danger',
              content: 'Inactive'              
            },
            {
              type: 'text',
              content: 'Tester'
            },
            {
              type: 'avatar',
              content: 'https://i2-prod.mirror.co.uk/incoming/article1820486.ece/ALTERNATES/s1227b/Phil-Jones.jpg',
              shape: 'circle',
              size: 'large'
            },
            {
              type: 'text',
              content: `Tester@gmail.com`,
            },
            {
              type: 'text',
              content: '081247564753'
            },
            {
              type: 'text',
              content: `Tester`

            },
            {
              type: 'text',
              content: '2022-12-06 16:53:02'
            },
            {
              type: 'text',
              content: `Tester`
            },
            {
              type: 'text',
              content: `2022-12-06 16:53:02`
            }
          ]
      },
      {
        id: 3,
        data: [
            {
              type: 'buttons',
              buttons : [
                {
                  id: 1,
                  text: 'Edit',
                  class: 'btn btn-primary',
                  props: {
                          module: 'Admin', 
                          subModule: 'Edit Administrator',
                          title: 'Edit Administrator',
                          backRoute: '/admin',
                          formData: this.formDataEdit
                         },
                  action: 'openPage',
                  icon: 'faEdit'
                },
                {
                  id: 2,
                  text: 'Set to Active',
                  class: 'btn btn-success',
                  icon: 'faCheck'
                }
              ]
            },
            {
              type: 'status',
              class: 'btn btn-danger',
              content: 'Inactive'              
            },
            {
              type: 'text',
              content: 'Tester'
            },
            {
              type: 'avatar',
              content: 'https://i2-prod.mirror.co.uk/incoming/article1820486.ece/ALTERNATES/s1227b/Phil-Jones.jpg',
              shape: 'circle',
              size: 'large'
            },
            {
              type: 'text',
              content: `Tester@gmail.com`,
            },
            {
              type: 'text',
              content: '081247564753'
            },
            {
              type: 'text',
              content: `Tester`

            },
            {
              type: 'text',
              content: '2022-12-06 16:53:02'
            },
            {
              type: 'text',
              content: `Tester`
            },
            {
              type: 'text',
              content: `2022-12-06 16:53:02`
            }
          ]
      },
      {
        id: 4,
        data: [
            {
              type: 'buttons',
              buttons : [
                {
                  id: 1,
                  text: 'Edit',
                  class: 'btn btn-primary',
                  props: {
                          module: 'Admin', 
                          subModule: 'Edit Administrator',
                          title: 'Edit Administrator',
                          backRoute: '/admin',
                          formData: this.formDataEdit
                         },
                  action: 'openPage',
                  icon: 'faEdit'
                },
                {
                  id: 2,
                  text: 'Set to Active',
                  class: 'btn btn-success',
                  icon: 'faCheck'
                }
              ]
            },
            {
              type: 'status',
              class: 'btn btn-danger',
              content: 'Inactive'              
            },
            {
              type: 'text',
              content: 'Tester'
            },
            {
              type: 'avatar',
              content: 'https://i2-prod.mirror.co.uk/incoming/article1820486.ece/ALTERNATES/s1227b/Phil-Jones.jpg',
              shape: 'circle',
              size: 'large'
            },
            {
              type: 'text',
              content: `Tester@gmail.com`,
            },
            {
              type: 'text',
              content: '081247564753'
            },
            {
              type: 'text',
              content: `Tester`

            },
            {
              type: 'text',
              content: '2022-12-06 16:53:02'
            },
            {
              type: 'text',
              content: `Tester`
            },
            {
              type: 'text',
              content: `2022-12-06 16:53:02`
            }
          ]
      },
      {
        id: 5,
        data: [
            {
              type: 'buttons',
              buttons : [
                {
                  id: 1,
                  text: 'Edit',
                  class: 'btn btn-primary',
                  props: {
                          module: 'Admin', 
                          subModule: 'Edit Administrator',
                          title: 'Edit Administrator',
                          backRoute: '/admin',
                          formData: this.formDataEdit
                         },
                  action: 'openPage',
                  icon: 'faEdit'
                },
                {
                  id: 2,
                  text: 'Set to Active',
                  class: 'btn btn-success',
                  icon: 'faCheck'
                }
              ]
            },
            {
              type: 'status',
              class: 'btn btn-danger',
              content: 'Inactive'              
            },
            {
              type: 'text',
              content: 'Tester'
            },
            {
              type: 'avatar',
              content: 'https://i2-prod.mirror.co.uk/incoming/article1820486.ece/ALTERNATES/s1227b/Phil-Jones.jpg',
              shape: 'circle',
              size: 'large'
            },
            {
              type: 'text',
              content: `Tester@gmail.com`,
            },
            {
              type: 'text',
              content: '081247564753'
            },
            {
              type: 'text',
              content: `Tester`

            },
            {
              type: 'text',
              content: '2022-12-06 16:53:02'
            },
            {
              type: 'text',
              content: `Tester`
            },
            {
              type: 'text',
              content: `2022-12-06 16:53:02`
            }
          ]
      },
      {
        id: 6,
        data: [
            {
              type: 'buttons',
              buttons : [
                {
                  id: 1,
                  text: 'Edit',
                  class: 'btn btn-primary',
                  props: {
                          module: 'Admin', 
                          subModule: 'Edit Administrator',
                          title: 'Edit Administrator',
                          backRoute: '/admin',
                          formData: this.formDataEdit
                         },
                  action: 'openPage',
                  icon: 'faEdit'
                },
                {
                  id: 2,
                  text: 'Set to Active',
                  class: 'btn btn-success',
                  icon: 'faCheck'
                }
              ]
            },
            {
              type: 'status',
              class: 'btn btn-danger',
              content: 'Inactive'              
            },
            {
              type: 'text',
              content: 'Tester'
            },
            {
              type: 'avatar',
              content: 'https://i2-prod.mirror.co.uk/incoming/article1820486.ece/ALTERNATES/s1227b/Phil-Jones.jpg',
              shape: 'circle',
              size: 'large'
            },
            {
              type: 'text',
              content: `Tester@gmail.com`,
            },
            {
              type: 'text',
              content: '081247564753'
            },
            {
              type: 'text',
              content: `Tester`

            },
            {
              type: 'text',
              content: '2022-12-06 16:53:02'
            },
            {
              type: 'text',
              content: `Tester`
            },
            {
              type: 'text',
              content: `2022-12-06 16:53:02`
            }
          ]
      },
      {
        id: 7,
        data: [
            {
              type: 'buttons',
              buttons : [
                {
                  id: 1,
                  text: 'Edit',
                  class: 'btn btn-primary',
                  props: {
                          module: 'Admin', 
                          subModule: 'Edit Administrator',
                          title: 'Edit Administrator',
                          backRoute: '/admin',
                          formData: this.formDataEdit
                         },
                  action: 'openPage',
                  icon: 'faEdit'
                },
                {
                  id: 2,
                  text: 'Set to Active',
                  class: 'btn btn-success',
                  icon: 'faCheck'
                }
              ]
            },
            {
              type: 'status',
              class: 'btn btn-danger',
              content: 'Inactive'              
            },
            {
              type: 'text',
              content: 'Tester'
            },
            {
              type: 'avatar',
              content: 'https://i2-prod.mirror.co.uk/incoming/article1820486.ece/ALTERNATES/s1227b/Phil-Jones.jpg',
              shape: 'circle',
              size: 'large'
            },
            {
              type: 'text',
              content: `Tester@gmail.com`,
            },
            {
              type: 'text',
              content: '081247564753'
            },
            {
              type: 'text',
              content: `Tester`

            },
            {
              type: 'text',
              content: '2022-12-06 16:53:02'
            },
            {
              type: 'text',
              content: `Tester`
            },
            {
              type: 'text',
              content: `2022-12-06 16:53:02`
            }
          ]
      },
      {
        id: 8,
        data: [
            {
              type: 'buttons',
              buttons : [
                {
                  id: 1,
                  text: 'Edit',
                  class: 'btn btn-primary',
                  props: {
                          module: 'Admin', 
                          subModule: 'Edit Administrator',
                          title: 'Edit Administrator',
                          backRoute: '/admin',
                          formData: this.formDataEdit
                         },
                  action: 'openPage',
                  icon: 'faEdit'
                },
                {
                  id: 2,
                  text: 'Set to Active',
                  class: 'btn btn-success',
                  icon: 'faCheck'
                }
              ]
            },
            {
              type: 'status',
              class: 'btn btn-danger',
              content: 'Inactive'              
            },
            {
              type: 'text',
              content: 'Tester'
            },
            {
              type: 'avatar',
              content: 'https://i2-prod.mirror.co.uk/incoming/article1820486.ece/ALTERNATES/s1227b/Phil-Jones.jpg',
              shape: 'circle',
              size: 'large'
            },
            {
              type: 'text',
              content: `Tester@gmail.com`,
            },
            {
              type: 'text',
              content: '081247564753'
            },
            {
              type: 'text',
              content: `Tester`

            },
            {
              type: 'text',
              content: '2022-12-06 16:53:02'
            },
            {
              type: 'text',
              content: `Tester`
            },
            {
              type: 'text',
              content: `2022-12-06 16:53:02`
            }
          ]
      },
      {
        id: 9,
        data: [
            {
              type: 'buttons',
              buttons : [
                {
                  id: 1,
                  text: 'Edit',
                  class: 'btn btn-primary',
                  props: {
                          module: 'Admin', 
                          subModule: 'Edit Administrator',
                          title: 'Edit Administrator',
                          backRoute: '/admin',
                          formData: this.formDataEdit
                         },
                  action: 'openPage',
                  icon: 'faEdit'
                },
                {
                  id: 2,
                  text: 'Set to Active',
                  class: 'btn btn-success',
                  icon: 'faCheck'
                }
              ]
            },
            {
              type: 'status',
              class: 'btn btn-danger',
              content: 'Inactive'              
            },
            {
              type: 'text',
              content: 'Tester'
            },
            {
              type: 'avatar',
              content: 'https://i2-prod.mirror.co.uk/incoming/article1820486.ece/ALTERNATES/s1227b/Phil-Jones.jpg',
              shape: 'circle',
              size: 'large'
            },
            {
              type: 'text',
              content: `Tester@gmail.com`,
            },
            {
              type: 'text',
              content: '081247564753'
            },
            {
              type: 'text',
              content: `Tester`

            },
            {
              type: 'text',
              content: '2022-12-06 16:53:02'
            },
            {
              type: 'text',
              content: `Tester`
            },
            {
              type: 'text',
              content: `2022-12-06 16:53:02`
            }
          ]
      },
      {
        id: 10,
        data: [
            {
              type: 'buttons',
              buttons : [
                {
                  id: 1,
                  text: 'Edit',
                  class: 'btn btn-primary',
                  props: {
                          module: 'Admin', 
                          subModule: 'Edit Administrator',
                          title: 'Edit Administrator',
                          backRoute: '/admin',
                          formData: this.formDataEdit
                         },
                  action: 'openPage',
                  icon: 'faEdit'
                },
                {
                  id: 2,
                  text: 'Set to Active',
                  class: 'btn btn-success',
                  icon: 'faCheck'
                }
              ]
            },
            {
              type: 'status',
              class: 'btn btn-danger',
              content: 'Inactive'              
            },
            {
              type: 'text',
              content: 'Tester'
            },
            {
              type: 'avatar',
              content: 'https://i2-prod.mirror.co.uk/incoming/article1820486.ece/ALTERNATES/s1227b/Phil-Jones.jpg',
              shape: 'circle',
              size: 'large'
            },
            {
              type: 'text',
              content: `Tester@gmail.com`,
            },
            {
              type: 'text',
              content: '081247564753'
            },
            {
              type: 'text',
              content: `Tester`

            },
            {
              type: 'text',
              content: '2022-12-06 16:53:02'
            },
            {
              type: 'text',
              content: `Tester`
            },
            {
              type: 'text',
              content: `2022-12-06 16:53:02`
            }
          ]
      },
      {
        id: 11,
        data: [
            {
              type: 'buttons',
              buttons : [
                {
                  id: 1,
                  text: 'Edit',
                  class: 'btn btn-primary',
                  props: {
                          module: 'Admin', 
                          subModule: 'Edit Administrator',
                          title: 'Edit Administrator',
                          backRoute: '/admin',
                          formData: this.formDataEdit
                         },
                  action: 'openPage',
                  icon: 'faEdit'
                },
                {
                  id: 2,
                  text: 'Set to Active',
                  class: 'btn btn-success',
                  icon: 'faCheck'
                }
              ]
            },
            {
              type: 'status',
              class: 'btn btn-danger',
              content: 'Inactive'              
            },
            {
              type: 'text',
              content: 'Tester'
            },
            {
              type: 'avatar',
              content: 'https://i2-prod.mirror.co.uk/incoming/article1820486.ece/ALTERNATES/s1227b/Phil-Jones.jpg',
              shape: 'circle',
              size: 'large'
            },
            {
              type: 'text',
              content: `Tester@gmail.com`,
            },
            {
              type: 'text',
              content: '081247564753'
            },
            {
              type: 'text',
              content: `Tester`

            },
            {
              type: 'text',
              content: '2022-12-06 16:53:02'
            },
            {
              type: 'text',
              content: `Tester`
            },
            {
              type: 'text',
              content: `2022-12-06 16:53:02`
            }
          ]
      },
    ]
 }

 searchInputData = [
  {
    id: 1,
    label: 'Name',
    type: 'input-text',
    inputType: 'text',
    placeholder: 'Name',
    value: ''
  },
  {
    id: 2,
    label: 'Email',
    type: 'input-text',
    inputType: 'email',
    placeholder: 'Email',
    value: ''
  },
  {
    id: 3,
    label: 'Phone',
    type: 'input-text',
    inputType: 'phone',
    placeholder: 'Phone',
    value: ''
  },
  {
    id: 4,
    label: 'Status',
    type: 'select-box',
    placeholder: 'Select Status',
    value: '',
    options: [
      {
        code: 'active',
        name: 'Active'
      },
      {
        code: 'inactive',
        name: 'Inactive'
      }
    ]
  }
 ]
}
